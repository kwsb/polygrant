PolyGrant

http://blog.polygrant.com

A crowdfunding platform for free cultural or charitable projects. Written in node.js and angular.js. 
Production configs, images, and payment code were removed. The code is a bit outdated now, but please feel free to fork it and use it!

Released under GPL 3.0.
