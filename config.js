var os = require('os');

var config = {};

// env -- 'development' or 'production'
config.env = 'development';


// Where public files are served
config.public_folder = "/node/public";


// file upload folder
config.upload_folder = config.public_folder + "/uploads";
config.upload_tmp = config.public_folder + "/uploads_tmp";
config.max_files = 20;


// In bytes
config.upload_filesize = 4009000;


// Email
config.email_noreply = "";
config.email_noreply_password = "";
config.email_support = "";
config.email_info = "";


// Password Reset link
config.reset_link = "";


// Contact Topic options
config.contact_options = ["My Account", "General Questions", "Project Support", "Payment", "DMCA", "Report Violation", "Other"];

// Elastic search info --- NOT USED
//config.elastic_index = "";


// Country Monetary Limits; country codes are ISO 3166-1 alpha 2
// List each limit in their respective country's currency
config.money_limits = {};
config.money_limits['US'] = 10000;
config.money_limits['GB'] = 5000;
config.money_limits['CA'] = 8000;
config.money_limits['AU'] = 8000;
config.money_limits['NZ'] = 8000;
config.money_limits['NL'] = 7000;
config.money_limits['DE'] = 7000;
config.money_limits['IE'] = 7000;
config.money_limits['SE'] = 50000;
config.money_limits['DK'] = 50000;
config.money_limits['NO'] = 50000;

// Supported Countries
var countries = require('country-data').countries;
config.supported_countries = ['US', 'GB', 'CA', 'AU', 'NZ', 'NL', 'DE', 'IE', 'SE', 'DK', 'NO'];

config.supported_currencies = config.supported_countries
    .map(function(x) { return countries[x].currencies[0]; })        // get currency of country
    .filter(function(x, index, array) { return array.indexOf(x) == index; })       // remove duplicate currencies

module.exports = config;
