// Run this as server for dev environment


    var express = require('express');
    var app = express();
    var jwt = require("jsonwebtoken");
    var expressJwt = require("express-jwt");
    var domain = require('domain');

    var config = require('./config');

    var session = require('express-session');

    var uuid = require('node-uuid');
    var logger = require('./utils/logger.js');
    var messages = require('./utils/messages.js');
    
    var path = require('path');

    app.use(express.static(config.public_folder));

    app.use(session({
        secret: '',     // this was loaded from elsewhere, but removed for this release
        saveUninitialized: true,
        resave: true
    }));


    // Routes
    app.use('/api', require('./routes/index'));
    app.use('/api/projects', require('./routes/project'));
    app.use('/api/stripe', require('./routes/stripe'));
    app.use('/api/search', require('./routes/search'));
    app.use('/api/user', require('./routes/user'));
    app.use('/api/admin', require('./routes/admin'));
    app.use('/api/comment', require('./routes/comment'));

    app.use('/*', function(req, res) {
        res.sendFile(path.join(config.public_folder + "/index.html"));
    });

    // This is Express errorhandler. It cannot catch errors happening in async code, so it bubbles up to domains and is handled there.
    app.use(function(err, req, res, next) {
        // csurf error
        if (err.code == 'EBADCSRFTOKEN') {
            res.status(401).json({"error": "session has expired or tampered with"});
            return;
        }

        // express-jwt error
        if (err.name === 'unauthorizedError') {
            res.status(401).json({"error": "session has expired"});
            return;
        }

        console.log(err.stack);
        console.log('Express error handler!');

    });


    // Create db - run this once
    var models = require('./models');
    var server = app.listen(3000, function() {
        models.sequelize.sync({logging: console.log}).then(function() {
            console.log("\n\n Express App started! \n\n");        
        });
    });
