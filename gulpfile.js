var gulp = require('gulp');
var usemin = require('gulp-usemin');
var uglify = require('gulp-uglify');
var minifyCss = require('gulp-minify-css');
var del = require('del');
var minifyHtml = require('gulp-minify-html');
var rev = require('gulp-rev');
var merge = require('merge-stream');

gulp.task('default', ['minifyPartials']);

gulp.task('clean', function() {
    var generated = ['production/*'];
    return del(generated);
});

gulp.task('minifyPartials', ['minify'], function() {
    return gulp.src(["public/partials/*.html"],  {base:'./'})
        .pipe(minifyHtml({empty: true, spare: true}))
        .pipe(gulp.dest("production"));
});

gulp.task('minify', ['copyOther'], function() {
    return gulp.src('public/index.html')
        .pipe(usemin({
            assetsDir: 'public',
            html: [minifyHtml({empty: true, spare: true})],
            cssLib: ['concat', rev()],
            cssCore: [minifyCss(), 'concat', rev()],
            jsLib: ['concat', rev()],
            jsCore: [uglify(), 'concat', rev()],
            jsButton: [uglify()]
        }))
        .pipe(gulp.dest('production/public'));
});


// Copy node app files
gulp.task('copy', ['clean'], function() {
    var filesToMove = ["./**/*"];
    var ignore = ['!public/**/*', '!gulpfile.js', '!production/**/*'];

    filesToMove = filesToMove.concat(ignore);

    return gulp.src(filesToMove, { base: './'})
        .pipe(gulp.dest('production'));
});

// copy other assets in public/
gulp.task('copyOther', ['copy'], function() {
    var filesToMove = [
        'public/images/**/*',
        'public/partials/**/*',
        'public/uploads/',
        'public/uploads_tmp/'
    ];

    var fonts = [
        'public/bower_components/bootstrap/dist/fonts/*',
        'public/bower_components/font-awesome/fonts/*'
    ];

    var hack = [
        'public/css/core/hack.css'
    ];

    var p = gulp.src(filesToMove, {base:'./'})
        .pipe(gulp.dest('production'));

    var f = gulp.src(fonts)
        .pipe(gulp.dest('production/public/fonts'));

    var h = gulp.src(hack, {base: './'})
        .pipe(minifyCss())
        .pipe(gulp.dest('production'));

    return merge(p,f, h);
});
