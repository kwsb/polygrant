"use strict";

module.exports = function(sequelize, DataTypes) {
    var Category = sequelize.define('category', {
        name: DataTypes.STRING,
        description: DataTypes.STRING,
        slug: DataTypes.STRING,
    }, {
        timestamps: false,
        classMethods: {
            associate: function(models) {
                Category.hasMany(models.project);
            }
        }
    });

    Category.afterUpdate(function(category, options, fn) {
        var utils = require('../utils/utils.js');
        var slug = utils.slugify(category.name);
        category.updateAttributes({ slug: slug }, {'hooks': false});
        fn(null, category);
    });

    Category.afterCreate(function(category, options, fn) {
        var utils = require("../utils/utils.js");
        var slug = utils.slugify(category.name);
        category.updateAttributes({ slug: slug }, {'hooks': false});
        fn(null, category);
    });

    return Category;
};
