"use strict";

var bcrypt = require('bcrypt');
var Promise = require('bluebird/js/main/promise')()
var config = require('../config.js');

module.exports = function(sequelize, DataTypes) {
  var Comment = sequelize.define("comment", {
    
    description: { type: DataTypes.STRING(1200), defaultValue: "", validate: { len: { args:[60,1200], msg:"comment needs 60-1200 characters" } } },

    token: { type: DataTypes.STRING, unique: true },
      
  }, {

    classMethods: {
        associate: function(models) {
            Comment.belongsTo(models.person);
            Comment.belongsTo(models.project);
        }
    }

  });

  Comment.beforeDestroy(function(comment, options) {
    var data = {};
 
    data['commentToken'] = comment.token;
    data['personToken'] = comment.person.token;
    data['projectToken'] = comment.project.token;
    data['description'] = comment.description;
    sequelize.models.commenthistory.create(data, {transaction: options.transaction});
  });

  Comment.beforeUpdate(function(comment, options) {
    var changed_fields = comment.changed();
    var data = {};
   
    if (changed_fields.length) {
        for(var i = 0; i<changed_fields.length; i++) {
            var key = changed_fields[i];
            data[key] = comment._previousDataValues[key];
        }
        data['commentToken'] = comment.token;
        data['personToken'] = comment.person.token;
        data['projectToken'] = comment.project.token;
        sequelize.models.commenthistory.create(data, {transaction: options.transaction});
    }
  });

  Comment.afterCreate(function(comment, options) {
    var utils = require("../utils/utils.js");
    var token = utils.genToken(7);
    comment.updateAttributes({ token: token }, {'hooks': false});
  });

  return Comment;
};
