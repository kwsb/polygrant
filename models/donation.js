"use strict";
var config = require('../config.js');

module.exports = function(sequelize, DataTypes) {
    var Donation = sequelize.define("donation", {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        amount: { 
            type: DataTypes.DECIMAL(10, 4), 
            validate: {
                min: 0
            }   
        },
        site_amount: {
            type: DataTypes.DECIMAL(10, 4),
            validate: {
                min: 0
            }
        },
        token: DataTypes.STRING,
        metadata: DataTypes.STRING,
        subtotal: {type: DataTypes.DECIMAL(10, 4), defaultValue: 0.0},
        total: {type: DataTypes.DECIMAL(10, 4), defaultValue: 0.0 },
        fee: {type: DataTypes.DECIMAL(10, 4), defaultValue: 0.0 },
        charge_id: DataTypes.STRING,         
        
        card_last4: DataTypes.STRING,

        currency: { type: DataTypes.STRING, defaultValue: 'USD', validate: { isIn: { args: [config.supported_currencies], msg:"invalid currency"} } },

        projectId: DataTypes.INTEGER,
        personId: DataTypes.INTEGER
    }, {
        classMethods: {
            associate: function(models) {
                Donation.belongsTo(models.person);
                Donation.belongsTo(models.project);
            }
        }
    });

    Donation.afterCreate(function(donation, options) {
        var utils = require("../utils/utils.js");
        var token = utils.genToken();
        donation.updateAttributes({ token: token }, {'hooks': false});
    });

    return Donation;
};

