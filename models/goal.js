"use strict";

module.exports = function(sequelize, DataTypes) {
    var Goal = sequelize.define('goal', {
        name: { type: DataTypes.STRING, validate: { len: { args: [1, 100], msg: "[goal] name needs to be 1-100 characters." } } },
        description: { type: DataTypes.STRING(1000), validate: { len: { args: [60, 1000], msg:"[goal] description needs to be 60-1000 characters." } } },
        amount: { type: DataTypes.INTEGER, defaultValue: 1, validate: { max: { args: 2147483647, msg: "[goal] funding needs to be less than 2147483647"}, min: { args: 1, msg: "[goal] needs to be at least 1."}, isInt: { args: true, msg: "[goal] funding needs to be a whole number"} } },

        token: { type: DataTypes.STRING, unique: true }, 
    }, {
        classMethods: {
            associate: function(models) {
                Goal.belongsTo(models.project);
            }
        }
    });


    Goal.beforeDestroy(function(goal, options) {

        var data = {};

        data['name'] = goal.name;
        data['description'] = goal.description;
        data['amount'] = goal.amount;

        data['projectToken'] = goal.project.token;
        data['goalToken'] = goal.token;
        sequelize.models.goalhistory.create(data, {transaction: options.transaction});
    });

    Goal.beforeUpdate(function(goal, options) {
        var changed_fields = goal.changed();
        var data = {};
 
        if (changed_fields.length) {
            for(var i = 0; i<changed_fields.length; i++) {
                var key = changed_fields[i];
                data[key] = goal._previousDataValues[key];
            }


            data['projectToken'] = goal.project.token;
            data['goalToken'] = goal.token;
            sequelize.models.goalhistory.create(data, {transaction: options.transaction});
        }
    });

    Goal.afterCreate(function(goal, options) {
        var utils = require("../utils/utils.js");
        var token = utils.genToken(10);
        goal.updateAttributes({ token: token }, {transaction: options.transaction, 'hooks': false});
    });

    return Goal;
};
