"use strict";

module.exports = function(sequelize, DataTypes) {
    var CommentHistory = sequelize.define('commenthistory', {
        description: DataTypes.STRING(1200),
        
        projectToken: DataTypes.STRING,
        commentToken: DataTypes.STRING,
        personToken: DataTypes.STRING
    });

    return CommentHistory;
};
