"use strict";

module.exports = function(sequelize, DataTypes) {
    var GoalHistory = sequelize.define('goalhistory', {
        name: DataTypes.STRING,
        description: DataTypes.STRING(1000),
        amount: DataTypes.INTEGER,

        projectToken: DataTypes.STRING,
        goalToken: DataTypes.STRING
    });

    return GoalHistory;
};
