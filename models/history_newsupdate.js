"use strict";

module.exports = function(sequelize, DataTypes) {
  var NewsUpdateHistory = sequelize.define("newsupdatehistory", {
    
    description: DataTypes.STRING(1000),
    
    newsupdateToken: DataTypes.STRING,
    projectToken: DataTypes.STRING
  });

  return NewsUpdateHistory;
};
