"use strict";

module.exports = function(sequelize, DataTypes) {
    var ProjectHistory = sequelize.define('projecthistory', {
        name: DataTypes.STRING,
        blurb: DataTypes.STRING,
        description: DataTypes.STRING(5000),
        desired_funding: DataTypes.INTEGER,
        project_url: DataTypes.STRING,
        icon_url: DataTypes.STRING,
        companies: DataTypes.ARRAY(DataTypes.STRING),
        money_description: DataTypes.STRING(1200),
        license: DataTypes.STRING,
        state: DataTypes.STRING,

        is_free_license: DataTypes.BOOLEAN,
        retire_date: DataTypes.DATE,

        slug: DataTypes.STRING,

        category: DataTypes.STRING,

        projectToken: DataTypes.STRING
    });

    return ProjectHistory;
};
