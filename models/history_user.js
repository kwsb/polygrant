"use strict";

module.exports = function(sequelize, DataTypes) {
  var PersonHistory = sequelize.define("personhistory", {
    name: DataTypes.STRING, 
    email: DataTypes.STRING,
    email_public: DataTypes.STRING,
        
    organization_type: DataTypes.STRING,
    tax_deductions: DataTypes.BOOLEAN,

    reputation: DataTypes.INTEGER,

    state: DataTypes.STRING,

    city_state: DataTypes.STRING,

    stripe_connected: DataTypes.BOOLEAN,
    
    country_origin: DataTypes.STRING,
    tax_exempt: DataTypes.BOOLEAN,

    currency: DataTypes.STRING,
    tax_deductions_policy: DataTypes.STRING(1000),
    
    bio: DataTypes.STRING(5000),

    personToken: DataTypes.STRING
   
  });

  return PersonHistory;
};
