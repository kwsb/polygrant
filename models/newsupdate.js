"use strict";

module.exports = function(sequelize, DataTypes) {
    var NewsUpdate = sequelize.define("newsupdate", {
        description: {type: DataTypes.STRING(1000),  validate: { len: { args: [1, 1000], msg:"news update needs 1-1000 characters." }}},
        token: DataTypes.STRING,
    }, {
        classMethods: {
            associate: function(models) {
                NewsUpdate.belongsTo(models.person);
                NewsUpdate.belongsTo(models.project);
            }
        }
    });

    NewsUpdate.beforeDestroy(function(newsupdate, options) {
        var data = {};

        data['description'] = newsupdate.description;
        data['newsupdateToken'] = newsupdate.token;
        data['projectToken'] = newsupdate.project.token;
        sequelize.models.newsupdatehistory.create(data, {transaction: options.transaction});
    });

    NewsUpdate.beforeUpdate(function(newsupdate, options) {
        var changed_fields = newsupdate.changed();
        var data = {};
 
        if (changed_fields.length) {
            for(var i = 0; i<changed_fields.length; i++) {
                var key = changed_fields[i];
                data[key] = newsupdate._previousDataValues[key];
            }
            data['newsupdateToken'] = newsupdate.token;
            data['projectToken'] = newsupdate.project.token;
            sequelize.models.newsupdatehistory.create(data, {transaction: options.transaction});
        }
    });

    NewsUpdate.afterCreate(function(newsupdate, options) {
        var utils = require("../utils/utils.js");
        var token = utils.genToken();
        newsupdate.updateAttributes({ token: token }, {'hooks': false});
    });

    return NewsUpdate;
};

