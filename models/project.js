"use strict";

module.exports = function(sequelize, DataTypes) {
    var Project = sequelize.define('project', {
        name: { type: DataTypes.STRING, validate: { len: { args: [1, 100], msg: "project name needs to be 1-100 characters." } } },
        blurb: { type: DataTypes.STRING, validate: { len: { args: [5, 100], msg: "blurb needs to be 5-100 characters." } } },
        description: { type: DataTypes.STRING(5000), validate: { len: { args: [60, 5000], msg:"project description needs to be 60-5000 characters." } } },
        desired_funding: { type: DataTypes.INTEGER, defaultValue: 0, validate: { max: { args: 2147483647, msg: "project funding needs to be less than 2147483647"}, min: { args: 0, msg: "project funding needs to be at least 0."}, isInt: { args: true, msg: "project funding funding needs to be a whole number"} } },
        project_url: { type: DataTypes.STRING, validate: { len: { args: [0, 100], msg:"project url needs to be 0-100 characters." } } },
        icon_url: { type: DataTypes.STRING, validate: { len: { args: [0, 100], msg:"icon url needs to be 0-100 characters." } } },
        video_url: { type: DataTypes.STRING, validate: { len: { args: [0, 100], msg:"video url needs to be 0-100 characters." } } },
        companies: { type: DataTypes.ARRAY(DataTypes.STRING), validate: { len: { args: [0, 100], msg:"companies / institutions needs to be 0-100 items" } } },
        money_description: { type: DataTypes.STRING(1200), validate: { len: { args: [0, 1200], msg:"expense description needs to be 0-1200 characters." } } },
        license: { type: DataTypes.STRING, validate: { len: { args: [0, 50], msg:"license needs to be 0-50 characters." } } },

        // "prelaunch", "pending approval", "launched", "retired", "disabled"
        state: { type: DataTypes.STRING, defaultValue: "prelaunch" },

        // TODO: need to validate array length + strings length (same for companies)
        images: { type: DataTypes.ARRAY(DataTypes.STRING)},

        is_free_license: { type: DataTypes.BOOLEAN, defaultValue: false },
        has_applied_free_license: { type: DataTypes.BOOLEAN, defaultValue: false },

        retire_date: { type:DataTypes.DATE },

        slug: DataTypes.STRING,
        token: { type: DataTypes.STRING, unique: true }, 
    }, {
        classMethods: {
            associate: function(models) {
                Project.hasMany(models.comment);
                Project.hasMany(models.newsupdate);
                Project.hasMany(models.donation);
                Project.belongsTo(models.person);
                Project.belongsToMany(models.person, {through: 'followings', as: 'followings'});
                Project.belongsTo(models.category);
                Project.hasMany(models.goal);
            }
        }
    });


    Project.beforeUpdate(function(project, options) {
        var changed_fields = project.changed();
        var data = {};
 

        if (changed_fields.length) {
            for(var i = 0; i<changed_fields.length; i++) {
                var key = changed_fields[i];
                data[key] = project._previousDataValues[key];
            }
            data['projectToken'] = project.token;
            sequelize.models.projecthistory.create(data, {transaction: options.transaction});
        }
    });

    Project.afterUpdate(function(project, options) {
        var utils = require('../utils/utils.js');
        var slug = utils.slugify(project.name);
        project.updateAttributes({ slug: slug }, {'hooks': false}, {transaction: options.transaction});
    });

    Project.afterCreate(function(project, options) {


        var utils = require("../utils/utils.js");
        var slug = utils.slugify(project.name);
        var token = utils.genToken(6);
        project.updateAttributes({ slug: slug, token: token }, {transaction: options.transaction, 'hooks': false});
    });

    return Project;
};
