"use strict";

var bcrypt = require('bcrypt');
var Promise = require('bluebird/js/main/promise')()
var config = require('../config.js');

module.exports = function(sequelize, DataTypes) {
  var Person = sequelize.define("person", {
    name: { type: DataTypes.STRING, validate: { 
            len: { args: [1, 100], msg:"name needs 1-100 characters." },
    } },
    email: { type: DataTypes.STRING, unique: true, validate: { 
        isEmail: { args: true, msg: "require valid email" },
        len: { args: [1, 100], msg:"email needs 1-100 characters."},
     } },
   
    email_public: { allowNull: true, type: DataTypes.STRING, unique: true, validate: {
        isEmail: { args: true, msg: "public contact needs a valid email" },
        len: { args: [0, 100], msg:"public contact has max 100 characters"},
     } },   
    
    organization_type: { type: DataTypes.STRING, defaultValue: 'Individual', validate: { len: { args:[1,50], msg:"organization type needs 1-50 characters" } } },
    country_origin: { type: DataTypes.STRING, defaultValue: 'US', validate: { isIn: { args: [config.supported_countries], msg: "invalid country" } } },
    city_state: { type: DataTypes.STRING(100), validate: { len: { args: [1, 255], msg:"city, state/province needs 1-255 characters" } } }, 
    
    tax_deductions: { type: DataTypes.BOOLEAN, defaultValue: false},
    tax_exempt: { type: DataTypes.BOOLEAN, defaultValue: false },
    // need approval for tax exempt entities
    has_applied_tax_exempt: { type: DataTypes.BOOLEAN, defaultValue: false},

    tax_deductions_policy: { type: DataTypes.STRING(1000), defaultValue: "" },
    
    bio: { type: DataTypes.STRING(5000), defaultValue: "", validate: { len: { args:[0,5000], msg:"biography needs 5000 characters or less" } } },

    reputation: { type: DataTypes.INTEGER, defaultValue: 0 },

    token: { type: DataTypes.STRING, unique: true },

    currency: { type: DataTypes.STRING, defaultValue: 'USD', validate: { isIn: { args: [config.supported_currencies], msg:"invalid currency"} } },

    // "approved", "disabled"
    state: { type: DataTypes.STRING, defaultValue: "approved" },

    // password resets
    reset_token: DataTypes.STRING,
    reset_expire: DataTypes.DATE,

    stripe_connected: { type: DataTypes.BOOLEAN, defaultValue: false },

    slug: DataTypes.STRING,
    
    // is admin
    is_admin: { type: DataTypes.BOOLEAN, defaultValue: false }, 
   
    password_hash: DataTypes.STRING, 
    password: { type: DataTypes.VIRTUAL, 
        validate: {
            len: { args: [8, 100], msg:"password requires 8-100 characters" },
        },

        set: function(password) {
            var salt = bcrypt.genSaltSync(15);
            var hash = bcrypt.hashSync(password, salt);

            this.setDataValue('password', password);
            this.setDataValue('password_hash', hash);
        }
    }
  }, {

    instanceMethods: {
        verifyPassword: function(password) {
            var this_password = this.password_hash;
            return new Promise(function(resolve, reject) {
                bcrypt.compare(password, this_password, function(err, isMatch) {
                    if (err) return reject(err);
                    if (!isMatch) return reject(new Error('Passwords do not match'));
                    resolve();
                });
            });
        },

        isDisabled: function() {
            return this.state == 'disabled';
        }
    },

    classMethods: {
        associate: function(models) {
            Person.hasMany(models.comment);
            Person.hasMany(models.newsupdate);
            Person.hasMany(models.donation);
            Person.hasMany(models.project);
            Person.belongsToMany(models.project, {through: 'followings', as: 'followings'});
        }
    }

  });

  Person.beforeUpdate(function(person, options) {
    var changed_fields = person.changed();
    var data = {};
   
    if (changed_fields.length) {
        for(var i = 0; i<changed_fields.length; i++) {
            var key = changed_fields[i];
            data[key] = person._previousDataValues[key];
        }
        data['personToken'] = person.token;
        sequelize.models.personhistory.create(data, {transaction: options.transaction});
    }
  });

  Person.afterCreate(function(person, options) {
    var utils = require("../utils/utils.js");
    var slug = utils.slugify(person.name);
    var token = utils.genToken(7);
    person.updateAttributes({ token: token, slug: slug }, {'hooks': false});
  });

  return Person;
};
