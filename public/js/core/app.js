
var app = angular.module('app', ['angulartics', 'angulartics.google.analytics', 'noCAPTCHA', 'ngAnimate', 'selectize', 'flow', 'nvd3', 'nvd3ChartDirectives', 'ui.router', 'uiRouterStyles', 'ui.bootstrap', 'ngRoute', 'controllers', 'services', 'directives', 'LocalStorageModule']);

var services = angular.module('services', []);
var controllers = angular.module('controllers', []);
var directives = angular.module('directives', []);

app.config(['flowFactoryProvider', function(flowFactoryProvider) {
    flowFactoryProvider.defaults = {
        target: '',
        permanentErrors:[404, 500, 501]
    };
}]);

var redirectIfAuthenticated = function(route) {
    return ['$location', '$q', 'SessionService', function($location, $q, SessionService) {
        var deferred = $q.defer();
        
        if (SessionService.getIsAuthenticated()) {
            deferred.reject();
            $location.path(route);
        } else {
            deferred.resolve();
        }

        return deferred.promise;
    }];
}

var loginRequired = ['$state', 'SessionService', '$q', '$timeout', function($state, SessionService, $q, $timeout) {
    var deferred = $q.defer();

    if(!SessionService.getIsAuthenticated()) {
        deferred.reject();
        $timeout(function() {
            $state.go('login');
        }, 10);
    } else {
        deferred.resolve();
    }
    
    return deferred.promise;
}]

var adminRequired = ['$state', 'SessionService', '$q', '$timeout', function($state, SessionService, $q, $timeout) {
    var deferred = $q.defer();

    if(!SessionService.getIsAdmin()) {
        deferred.reject();
        $timeout(function() {
            $state.go("projects");
        }, 10);
    } else {
        deferred.resolve();
    }

    return deferred.promise;
}]


// Routing Setup
function appRouteConfig($stateProvider, $urlRouterProvider, $locationProvider) {

    $locationProvider.html5Mode({
        enabled: true,
        requireBase: false    
    });


    // No route matches
    $urlRouterProvider.otherwise("/404");
    $stateProvider.
    state('publicprofile', {
        url: '/user/:token/:name',
        controller: 'PublicProfileCtrl',
        templateUrl: 'partials/profile_public.html'
    }).
    state('projects', {
        url: '/projects?page&category&filter&te&fcw',
        controller: 'MainCtrl',
        templateUrl: 'partials/main.html',
    }).
    state('main', {
        url: '/',
        controller: 'LandingCtrl',
        templateUrl: 'partials/landing.html',
        data: {
            css: 'css/core/hack.css'
        }
    }).
    state('newproject', {
        url: '/projects/new',
        controller: 'ProjectCtrl',
        templateUrl: 'partials/project_new.html',
        resolve: { loginRequired: loginRequired }
    }).
    state('editproject', {
        url: '/projects/edit/:token',
        controller: 'ProjectCtrl',
        templateUrl: 'partials/project_edit.html',
        resolve: { loginRequired: loginRequired }
    }).
    state('viewproject', {
        url: '/projects/view/:token/:slug',
        controller: 'ProjectCtrl',
        templateUrl: 'partials/project_view.html',
    }).
    state('login', {
        url: '/login',
        controller: 'UserCtrl',
        templateUrl: 'partials/login.html',
        resolve: { redirectIfAuthentiated: function() { return redirectIfAuthenticated('/main'); }}
    }).
    state('logout', {
        url: '/logout',
        controller: 'LogoutCtrl',
        template: ''
    }).
    state('register', {
        url: '/register',
        controller: 'UserCtrl',
        templateUrl: 'partials/register.html',
        resolve: { redirectIfAuthenticated: function() { redirectIfAuthenticated('/main'); }}
    }).
    state('profile', {
        url: '/profile',
        controller: 'ProfileCtrl',
        templateUrl: 'partials/profile.html',
        resolve: { loginRequired: loginRequired }
    }).
    state('stripeconnect', {
        url: '/stripe/connect/status',
        controller: 'StripeConnectCtrl',
        templateUrl: 'partials/stripe_connect.html'
    }).
    state('search', {
        url: '/search?terms&page&category&filter&te&fcw',
        controller: 'SearchCtrl',
        templateUrl: 'partials/search.html',
    }).
    state('forgot', {
        url: '/forgot',
        controller: 'UserCtrl',
        templateUrl: 'partials/forgot.html',
    }).
    state('reset', {
        url: '/reset/:token',
        controller: 'UserCtrl',
        templateUrl: 'partials/reset.html',
    }).
    state('faq', {
        url: '/faq',
        controller: 'StaticCtrl',
        templateUrl: 'partials/faq.html',
    }).
    state('faq_general', {
        url: '/faq/general',
        controller: 'StaticCtrl',
        templateUrl: 'partials/faq_general.html',
    }).
    state('faq_creator', {
        url: '/faq/creator',
        controller: 'StaticCtrl',
        templateUrl: 'partials/faq_creator.html',
    }).
    state('faq_donator', {
        url: '/faq/donator',
        controller: 'StaticCtrl',
        templateUrl: 'partials/faq_donator.html',
    }).
    state('faq_basics', {
        url: '/faq/basics',
        controller: 'StaticCtrl',
        templateUrl: 'partials/faq_basics.html',
    }).
    state('dmca', {
        url: '/dmca',
        controller: 'StaticCtrl',
        templateUrl: 'partials/dmca.html',
    }).
    state('privacy', {
        url: '/privacy',
        controller: 'StaticCtrl',
        templateUrl: 'partials/privacy.html',
    }).
    state('tos', {
        url: '/tos',
        controller: 'StaticCtrl',
        templateUrl: 'partials/tos.html',
    }).
    state('pmow', {
        url: '/pmow',
        controller: 'StaticCtrl',
        templateUrl: 'partials/pmow.html',
    }).
    state('404', {
        url: '/404',
        controller: 'StaticCtrl',
        templateUrl: 'partials/404.html',
    }).
    state('contact', {
        url: '/contact',
        controller: 'ContactCtrl',
        templateUrl: 'partials/contact.html',
    }).
    state('payment_success', {
        url: '/payment_success', 
        controller: 'ReceiptCtrl',
        templateUrl: 'partials/payment_success.html',
    }).
    state('admin', {
        url: '/admin/:service/:name',
        controller: 'AdminCtrl',
        templateUrl: 'partials/admin.html',
        resolve: { adminRequired: adminRequired }
    });
    
}

app.config(['$stateProvider', '$urlRouterProvider', '$locationProvider', appRouteConfig]);

app.config(['$httpProvider', function($httpProvider) {
    $httpProvider.interceptors.push('TokenInterceptor');
}]);
