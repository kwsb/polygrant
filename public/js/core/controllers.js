controllers.controller('PublicProfileCtrl', ['$scope', '$sce', '$stateParams', 'SeoService', 'UserService', 'SessionService',
    function($scope, $sce, $stateParams, SeoService, UserService, SessionService) {
        $scope.current_user = SessionService.getIsAuthenticated();
        $scope.profile = {
            bio: "",
            projects: []
        };
        
        $scope.md = window.markdownit();
        $scope.renderMarkdown = function(item) {
            return $sce.trustAsHtml($scope.md.render(item));
        }

        $scope.init = function() {
            var token = $stateParams.token;
            UserService.getPublicProfile(token).success(function(data) {
                $scope.profile = data['person'];
                SeoService.setData($scope.profile.name, $scope.profile.bio);
            });
        }

        $scope.init();
}]);


controllers.controller('AdminCtrl', ['$scope', '$stateParams', 'SessionService', 'AdminService', 'UtilService',
    function($scope, $stateParams, SessionService, AdminService, UtilService) {
        $scope.current_user = SessionService.getIsAuthenticated();
        $scope.adminuser = SessionService.getIsAdmin();
        $scope.ready = false;

        $scope.projects = null;
        $scope.users = null;

        $scope.show_projects = false;
        $scope.show_users = false;
        $scope.mode = "";
        $scope.service = "";

        $scope.getObjectKeys = function(object) {
            return Object.keys(object);
        }

        $scope.serviceProject = function(index, id, service) {
            UtilService.getCsrf().success(function() {
               AdminService.serviceProject(id, service).success(function(data) {
                   $scope.projects[index] = data['project'];
               });
            });
        }

        $scope.serviceUser = function(index, id, service) {
             UtilService.getCsrf().success(function() {
                AdminService.serviceUser(id, service).success(function(data) {
                    $scope.users[index] = data['user'];
                });
             });
        }

        $scope.init = function() {
            $scope.mode = $stateParams.name;
            $scope.service = $stateParams.service;

            if ($scope.mode == 'projects') {
                AdminService.getProjects($scope.service).success(function(data) {
                    $scope.projects = data['projects'];
                    $scope.show_projects = true;
                    $scope.ready = true;
                });
            } else if ($scope.mode == 'users') {
                AdminService.getUsers($scope.service).success(function(data) {
                    $scope.users = data['users'];
                    $scope.show_users = true;
                    $scope.ready = true;
                });
            }
        }

        $scope.init();
}]);

controllers.controller('LandingCtrl', ['$scope', 'SessionService', 'SeoService',
    function($scope, SessionService, SeoService) {
        $scope.current_user = SessionService.getIsAuthenticated();
        SeoService.setData("PolyGrant", "PolyGrant is a crowdfunding and discovery platform for projects with community benefit. Help crowdfund scientific research, free/open source works, disaster relief, charities, political activism, and more");
}]);

controllers.controller('SearchCtrl', ['$scope', '$state', '$stateParams', '$location', 'UtilService', 'SessionService',
    function($scope, $state, $stateParams, $location, UtilService, SessionService) {
        $scope.current_user = SessionService.getIsAuthenticated();
        $scope.ready = false;
        $scope.date = new Date();

        $scope.countries = {};
        $scope.currencies = {};

        // pagination
        $scope.pageCurrent = 1;
        $scope.pageMaxItems = 1;
        $scope.pageDisplayItems = 30;

        // filter
        $scope.filter = "";
        $scope.filterCategory = "";
        $scope.filterFreeWorks = false;
        $scope.filterTaxExempt = false;

        $scope.filterTaxExemptChange = function() {
            $scope.filterTaxExempt = !$scope.filterTaxExempt;
            if ($scope.filterTaxExempt == false) {
                $location.search('te', null);
            } else {
                $location.search('te', $scope.filterTaxExempt.toString());
            }
            $location.path('search');
        }

        $scope.filterFreeWorksChange = function() {
            $scope.filterFreeWorks = !$scope.filterFreeWorks;
            if ($scope.filterFreeWorks == false) {
                $location.search('fcw', null);
            } else {
                $location.search('fcw', $scope.filterFreeWorks.toString());
            }
            $location.path('search');
        }

        $scope.filterChange = function() {
            $location.search('filter', $scope.filter);
            $scope.doSearch();
        }
         
        $scope.filterCategoryChange = function() {
            $location.search('category', $scope.filterCategory);
            $scope.doSearch();
        }

        $scope.searchterms = "";
        $scope.projects = [];
        $scope.categories = [];

        $scope.init = function() {
            UtilService.getCategories().success(function(data) {
                $scope.categories = data['data'];
            });

            $scope.getCountriesCurrencies();
            $scope.filter = $stateParams.filter || "newest";
            $scope.filterCategory = $stateParams.category || "";
            $scope.filterFreeWorks = $stateParams.fcw === "true";
            $scope.filterTaxExempt = $stateParams.te === "true";
            var page = parseInt($stateParams.page);
            $scope.pageMaxItems = page * $scope.pageDisplayItems;
            $scope.pageCurrent = page;
            $scope.searchterms = $stateParams.terms || "";
            $scope.doSearch();
        }

        $scope.viewProject = function(token, slug) {
            $location.url($location.path());
            $location.path("/projects/view/"+token+"/"+slug);
        }

        $scope.getCountriesCurrencies = function() {
            UtilService.getCountriesCurrencies().success(function(data) {
                $scope.countries = data['countries'];
                $scope.currencies = data['currencies'];
            });
        }

        $scope.doSearch = function() {
            UtilService.searchProjects({"page": $stateParams.page, "terms": $scope.searchterms, "category": $scope.filterCategory, "filter": $scope.filter, "te": $scope.filterTaxExempt, "fcw": $scope.filterFreeWorks}).success(function(data) {

// Old ES code
/*
                $scope.projects = data["data"].map(function(project) {
                    if (project["_source"])
                        return project["_source"];
                });
*/
                
                // !! remove this line if using ES !!
                $scope.projects = data['data'];

                $scope.pageMaxItems = parseInt(data["total"]);
                $scope.ready = true;
            });
        }

        $scope.pageChanged = function() {
            $location.search('page', $scope.pageCurrent);
            $scope.doSearch();
        }

        $scope.init();
}]);

controllers.controller('StripeConnectCtrl', ['$scope', '$stateParams', '$location', 'StripeService', 'SessionService', 'UtilService',
    function($scope, $stateParams, $location, StripeService, SessionService, UtilService) {
        $scope.current_user = SessionService.getIsAuthenticated();

        $scope.connected = false;
        $scope.waiting = true;
        $scope.errors = [];

        $scope.init = function() {
            var response = $location.search();
            if (response.error) {
                $scope.connected = false;
                $scope.waiting = false;
            } else if (response.code) {
                UtilService.getCsrf().success(function() {
                    StripeService.setupConnect(response.code).success(function(data) {
                        $scope.connected = true;
                        $scope.waiting = false;
                        SessionService.setToken(data['token']);
                        SessionService.setUserInfo(data['data']);
                    }).error(function(err) {
                        $scope.connected = false;
                        $scope.waiting = false;
                        $scope.errors = err['errors'];
                    });
                });
            }
        }
        
        $scope.redirectProfile = function() {
            $location.url($location.path());
            $location.path("/profile");
        }

        $scope.init();

}]);
controllers.controller('ProfileCtrl', ['$sce', '$window', '$timeout', '$scope', '$modal', '$stateParams', '$location', 'UserService', 'SessionService', 'UtilService', 'ProjectService',
    function($sce, $window, $timeout, $scope, $modal, $stateParams, $location, UserService, SessionService, UtilService, ProjectService) {
        $scope.current_user = SessionService.getIsAuthenticated();
        $scope.adminuser = SessionService.getIsAdmin();
        $scope.ready = false;
        $scope.errors = [];
        $scope.success = false;        
        $scope.show_spinner = false;
        $scope.show_spinner_following = false;
        $scope.show_tax_deductions_policy = false;

        $scope.countries = {};

        $scope.show_stripe_connect = true;
         
        $scope.profile = {
            name: "",
            email: "",
            email_public: "",
            token: "",
            tax_deduction: false,
            tax_exempt: false,
            country_origin: "",
            city_state: "",
            organization_type: "",
            stripe_connected: false,
            password: "",
            tax_deductions_policy: "",
            currency: "USD",
            bio: "",
            slug: "",
            state: ""
        };
    
        $scope.profile_edit = {
            email: "",
            email_public: "",
            current_password: "",
            password: "",
            tax_deductions: false,
            organization_type: "",
            tax_exempt: false,
            country_origin: "",
            city_state: "",
            tax_deductions_policy: "",
            currency: "USD",
            bio: "",
            slug: "",
            state: ""
        };


        $scope.projects_stats = {};
        
        // Pie Chart
        $scope.chart_total = {};
        $scope.chart_total.options = {
            chart: {
                type: 'pieChart',
                height: 350,
                width: 350,
                x: function(d){return d.key;},
                y: function(d){return d.y;},
                showLabels: true,
                transitionDuration: 500,
                labelThreshold: 0.01,
                legend: {
                    margin: {
                        top: 5,
                        right: 35,
                        bottom: 5,
                        left: 0
                    }
                },
                tooltips: false,
                donut: true,
                donutLabelsOutside: true,
                growOnHover: false,
                showLegend: false,
                donutRatio: .2
            }
        };
        $scope.chart_total.data = [];
        
        $scope.select = function () {
            $timeout(function () {
                $scope.chart_total.api.update();
            });
        };
        
        // ------------

        $scope.open = function(size) {
            $scope.success = false;
            var modalInstance = $modal.open({
                templateUrl: 'profileModal.html',
                controller: 'ModalInstanceCtrl',
                size: size,
                resolve: {
                    profile_edit: function() {
                        return $scope.profile_edit;
                    },
                    countries: function() {
                        return $scope.countries;
                    },
                }
            });

            modalInstance.result.then(function(data) {
                var updated_profile = angular.copy(data['data']); 
                var token = data['token'];

                $scope.profile = updated_profile;
                SessionService.setUserInfo(updated_profile);
                SessionService.setToken(token);

                $scope.success = true;

            });
        }

        $scope.openProjectShareLink = function(project_token, project_slug) {
            var modalInstance = $modal.open({
                templateUrl: "projectShareModal.html",
                controller: "ProjectShareModalCtrl",
                size: "",
                resolve: {
                    project_token: function() {
                        return project_token;
                    },
                    project_slug: function() {
                        return project_slug;
                    }
                }
            });
        }


        $scope.md = window.markdownit();
        $scope.renderMarkdown = function(item) {
            return $sce.trustAsHtml($scope.md.render(item));
        }

        $scope.toggleTaxDeductionsPolicy = function() {
            $scope.show_tax_deductions_policy = !$scope.show_tax_deductions_policy;
        }

        $scope.unfollow = function(project_token) {
            $scope.show_spinner_following = true;
            UtilService.getCsrf().success(function() {
                ProjectService.submitFollow(project_token).success(function() {
                    $scope.queryData();
                    $scope.errors = [];
                }).error(function() {
                    $scope.errors = ['Something went wrong!']
                    $scope.show_spinner_following = false;   
                });
            });
        }

        $scope.toggle_stripe_connect = function() {
            $scope.show_stripe_connect = !$scope.show_stripe_connect;
        }

        $scope.launch = function(project) {
            var project_token = project.token;
            UtilService.getCsrf().success(function() {
                ProjectService.launchProject(project_token).success(function(data) {
                    var index = $scope.profile.projects.indexOf(project);

                    $scope.profile.projects[index].state = data['project_state'];
                });
            });
        }

        $scope.queryData = function() {
            UserService.getProfile().success(function(data) {
                UtilService.getCountriesCurrencies().success(function(cdata) {
                    $scope.projects_stats = data['projects_stats'];

                    // load chart data
                    for(var k in $scope.projects_stats) {
                        var stat = $scope.projects_stats[k];
                        $scope.chart_total.data.push({key: stat.name, y: stat.total});
                    }

                    var p = data['data'];
                    $scope.profile = p;
                    $scope.profile_edit = angular.copy(p);
                    $scope.profile_edit.tax_exempt = $scope.profile_edit.tax_exempt || $scope.profile_edit.has_applied_tax_exempt;
                    $scope.countries = cdata['countries'];
                    $scope.ready = true;
                    $scope.show_spinner_following = false;
                    $scope.show_stripe_connect = !p['stripe_connected'];
    
                });
            });
            
        }

        $scope.queryData();

}]);

controllers.controller("ProjectShareModalCtrl", ["$scope", "$modalInstance", "project_token", "project_slug", function($scope, $modalInstance, project_token, project_slug) {
    $scope.project_linkshare = '<p><script type="text/javascript" src="https://polygrant.com/js/button.js"></script><script type="text/javascript">polygrantButton(\'' + project_token + "', '" + project_slug + "');</script></p>";

    $scope.okay = function() {
        $modalInstance.close()
    }
}]);

controllers.controller('ModalInstanceCtrl', ['$scope', '$modalInstance', 'SessionService', 'UtilService', 'UserService','profile_edit', 'countries',  function($scope, $modalInstance, SessionService, UtilService, UserService, profile_edit, countries) {
    $scope.profile_edit = profile_edit;
    $scope.countries = countries;

    $scope.modal = {};
    
    // angular-selectize2
    $scope.modal.selectize = {};
    $scope.modal.selectize.config = {
        options: Object.keys(countries).map(function(x) { return { value: countries[x].code, text: countries[x].name }; }),
        create: true,
        sortField: 'text',
        maxItems: 1
    };

    $scope.modal.errors = [];
    $scope.modal.show_spinner = false;
    $scope.modal.success = false;
    $scope.modal.ready = false;
    
    $scope.resetProfileEdit = function() {
        $scope.profile_edit.current_password = "";
        $scope.profile_edit.password = "";
        $scope.profile_edit.key = "";
    }
   
    
    $scope.save = function() {

        UtilService.getCsrf().success(function() {
            $scope.modal.errors = [];
            $scope.modal.show_spinner = true;
            UserService.updateProfile(profile_edit).success(function(data) {

                $scope.modal.success = true;
                $scope.modal.errors = [];

                $scope.modal.show_spinner = false;
                $scope.resetProfileEdit();

                $modalInstance.close(data);

            }).error(function(errors) {
                $scope.modal.errors = errors['errors'] || ['Something went wrong!'];
                $scope.modal.success = false;
                $scope.modal.show_spinner = false;

                $scope.resetProfileEdit();
            });
        });
    }
    $scope.cancel = function() {
        $modalInstance.dismiss('cancel');
    }
}]);

controllers.controller('ReceiptCtrl', ['$scope', '$location', 'UtilService', 'SessionService', 'ReceiptService', 
    function($scope, $location, UtilService, SessionService, ReceiptService) {
        $scope.current_user = SessionService.getIsAuthenticated();
        $scope.donation = null;
        $scope.success = false;
        $scope.countries = {};

        $scope.printReceipt = function() {
            window.print();
        }

        $scope.init = function() {
            var token = ReceiptService.getDonationToken();
            UtilService.getDonation(token).success(function(data) {
                UtilService.getCountriesCurrencies().success(function(cdata) {
                    $scope.donation = data['donation'];
                    $scope.donation.createdAt = Date.parse($scope.donation.createdAt);
                    $scope.countries = cdata['countries'];
                    $scope.success = true;
                });
            });
        }

        $scope.init();
}]);

controllers.controller('ProjectCtrl', ['$window', '$sce', '$scope', '$stateParams', '$location', '$timeout', 'CommentService', 'UtilService', 'UserService', 'SessionService', 'ProjectService', 'StripeService', 'ReceiptService', 'SeoService',
    function($window, $sce, $scope, $stateParams, $location, $timeout, CommentService, UtilService, UserService, SessionService, ProjectService, StripeService, ReceiptService, SeoService) {
        $scope.current_user = SessionService.getIsAuthenticated();
        $scope.current_user_data = SessionService.getUserInfo();
        $scope.ready = false;
        $scope.date = new Date();
        $scope.errors = [];
        $scope.errors_newsupdate = {};
        $scope.errors_comment = {};
        $scope.errors_image = {};
        $scope.success = false;
        $scope.is_followed = false;
        $scope.payment_waiting = false;     // When user clicks to pay, use this to lock the screen.


        $scope.video = "";

        $scope.categories = [];
        $scope.currencies = {};
        $scope.countries = {};

        $scope.comment_new = { "description": ""};
        $scope.comment_edit = {};
         
        // tabs
        $scope.active_tabs = [true, false, false, false, false];
        
        // image slides
        $scope.slides = [];


        $scope.tmp_imgs = [];        // used for loading imgs in user's tmp folder when creating new project
        $scope.new_goals = [];
        $scope.existing_goals = [];     // this will hold all current changes
        $scope.project = {
            name: "",
            description: "",
            desired_amount: "",
            retired: false,
            is_free_license: false,
            license: "",
            icon_url: "",
            project_url: "",
            video_url: "",
            token: "",
            companies: [],
            money_description: "",
            blurb: "",
            images: [],
            person: {tax_deductions_policy: "", currency: "USD", country_origin: ""},
            goals: [],
            state: "",
            comments: []
        };
        $scope.images_new = [];

        $scope.newsupdate_new = {"description": ""};    // tabset has its own scope... use dot notation to access this model
        $scope.newsupdate_edit = [];

        $scope.donations_total = "";

        $scope.mode = "";
       
        $scope.md = window.markdownit();
        $scope.renderMarkdown = function(item) {
            return $sce.trustAsHtml($scope.md.render(item));
        }

        $scope.uploader = {flow : ""};
        $scope.uploader_imagehash = {};  // map original filename to image hash returned from server

        $scope.donation = {
            amount: 0,
            site: 0,
            project: 0
        };
        $scope.donation_success = null;

        $scope.donation_project_percentage = 100;
        $scope.donation_site_percentage = 0;
        
        $scope.isValidDonationAmount = function() {
            return $scope.donation.amount % 1 == 0 && $scope.donation.amount > 0;
        }
        
        $scope.changeDonation = function(type) {
            var money_limit = 0;

            // for some reason this function gets run when slider gets rendered and before data loads, so this is a crappy workaround, otherwise you'll set 'undefined' for money_limit
            if ($scope.ready)
                money_limit = $scope.countries[$scope.project.person.country_origin].money_limit;
            
            // restrict to money max amount
            if ($scope.donation.amount > money_limit)
                $scope.donation.amount = money_limit;

            if (type == 'site') 
                $scope.donation_project_percentage = 100 - $scope.donation_site_percentage;
            if (type == 'project')
                $scope.donation_site_percentage = 100 - $scope.donation_project_percentage;

            $scope.donation.project = Math.round($scope.donation.amount * ($scope.donation_project_percentage/100));
            $scope.donation.site = $scope.donation.amount - $scope.donation.project;
        }


        $scope.categoryClick = function(category_name) {
            $location.search('category', category_name);
            $location.path('projects');        
        }


        $scope.createComment = function() {
            UtilService.getCsrf().success(function() {
                CommentService.createComment($scope.project.token, $scope.comment_new.description).success(function(data) {
                    $scope.project.comments.unshift(data['comment']);
                    $scope.comment_new.description = "";
                    $scope.errors_comment['newcomment'] = [];
                }).error(function(err) {
                    $scope.errors_comment['newcomment'] = err['errors'];
                });
            });
        }

        $scope.isAuthorizedComment = function(person_token) {
            return $scope.current_user && person_token == $scope.current_user_data['token'];
        }
        
        $scope.toggleCommentEdit = function(comment_id) {
            $scope.comment_edit[comment_id] = !$scope.comment_edit[comment_id];
        }

        $scope.deleteComment = function(comment, comment_id) {
            UtilService.getCsrf().success(function() {
                CommentService.deleteComment(comment_id).success(function() {
                    var index = $scope.project.comments.indexOf(comment);
                    $scope.project.comments.splice(index, 1);

                    $scope.errors_comment[comment_id] = [];
                }).error(function(err) {
                    $scope.errors_comment[comment_id] = err['errors'];
                });
            });
        }

        $scope.editComment = function(comment_id, description) {
            UtilService.getCsrf().success(function() {
                CommentService.editComment(comment_id, description).success(function() {
                    $scope.toggleCommentEdit(comment_id);
                    $scope.errors_comment[comment_id] = [];
                }).error(function(err) {
                    $scope.errors_comment[comment_id] = err['errors'];
                });
            });
        }



        $scope.updateNews = function() {
            UtilService.getCsrf().success(function() {
                ProjectService.submitNews($scope.project.token, "", $scope.newsupdate_new.description).success(function(data) {
                    $scope.project.newsupdates = data['newsupdates'];
                    $scope.newsupdate_new.description= "";
                    $scope.errors = [];
                }).error(function(err) {
                    $scope.errors = err['errors'];
                });
            });
        }

        $scope.deleteUpdateNews = function(index, id) {
            UtilService.getCsrf().success(function() {
                ProjectService.deleteNews(id).success(function() {
                    $scope.project.newsupdates.splice(index, 1);
                    $scope.toggleUpdateNewsEdit(index);
                });
            });
        }

        $scope.editUpdateNews = function(index, id, description) {
            UtilService.getCsrf().success(function() {
                ProjectService.submitNews($scope.project.token, id, description).success(function(data) {
                    $scope.toggleUpdateNewsEdit(index); 
                    delete $scope.errors_newsupdate[id];
                }).error(function(err) {
                    $scope.errors_newsupdate[id] = err['errors'];
                });
            });
        }

        $scope.follow = function() {
            var user = SessionService.getUserInfo();
            UtilService.getCsrf().success(function() {
                ProjectService.submitFollow($scope.project.token).success(function() {
                    $scope.is_followed = !$scope.is_followed;            
                });
            });
        }

        $scope.isNoFee = function() {
            return $scope.project.person.tax_exempt == true || $scope.project.is_free_license == true;
        }

        $scope.toggleUpdateNewsEdit = function(index) {
            $scope.newsupdate_edit[index] = !$scope.newsupdate_edit[index];
        }

        $scope.isAuthorized = function() {
            if($scope.ready)
                return $scope.current_user && $scope.current_user_token == $scope.project.person.token ? true : false;
            return false;
        }


        $scope.addNewGoal = function() {
            $scope.new_goals.push({
                name: '',
                description: '',
                amount: ""
            });
        }

        $scope.deleteNewGoal = function(index) {
            $scope.new_goals.splice(index, 1);
        }

        $scope.deleteExistingGoal = function(goal_token, index) {
            UtilService.getCsrf().success(function() {
                ProjectService.deleteExistingGoal(goal_token).success(function() {
                    $scope.existing_goals.splice(index, 1);
                });
            });
        }

        $scope.init = function(mode) {
            $scope.mode = mode;
            $scope.getCountriesCurrencies();
                
            $scope.current_user_token = SessionService.getUserInfo();
            if ($scope.current_user_token) {
                $scope.current_user_token = $scope.current_user_token['token'];
            }
            if (mode == "edit" || mode == "view") {
                var token = $stateParams.token;
                ProjectService.getProject(token).success(function(data) {

                    $scope.project = data['data'];
                    $scope.existing_goals = angular.copy(data['data'].goals);

                    SeoService.setData($scope.project.name, $scope.project.blurb);

                    $scope.slides = $scope.project.images.map(function(x) { return {'image': x}; });
                    $scope.donations_total = parseInt(data['donations'][0]['donation_sum']) || 0;

                    $scope.video_url = $sce.trustAsResourceUrl($scope.project.video_url);

                    if (mode == 'edit') {
                        // temporarily set disabled to true/false depending on state
                        $scope.project.retired = $scope.project.state == 'retired' ? true : false;

                        // temporarily set free license to true if they've applied but have not yet been approved
                        $scope.project.is_free_license = $scope.project.is_free_license || $scope.project.has_applied_free_license;
                    }
                    
                    // Calculate donation percent defaults
                    var nofee = $scope.isNoFee();
                    $scope.donation_project_percentage = nofee ? 95 : 100;
                    $scope.donation_site_percentage = nofee ? 5 : 0;


                    // Setup stripe opts
                    $scope.handler = StripeCheckout.configure({
                        key: "pk_test_4QwKGQWlhUaZrwkDyMlco9dn",
                        zipcode: true,
                        address: true,
                        image: "images/siphonophore4_128.png",
                        name: "PolyGrant",
                        description: "Donation to " + $scope.project.name + " project on PolyGrant.com",
                        currency: $scope.project.person.currency,
                        token: function(token) {
                            $scope.payment_waiting = true;
                            $scope.show_spinner = true;
                            UtilService.getCsrf().success(function() {
                                StripeService.charge($scope.project.token, $scope.donation, token).success(function(data) {
                                    ReceiptService.setDonationToken(data['donation']);
                                    $scope.show_spinner = false;
                                    $location.path("/payment_success");
                                }).error(function(err) {
                                    $scope.show_spinner = false;
                                    $scope.payment_waiting = false;
                                    $scope.donation_success = false;
                                });
                            });
                        }
                    });

                    if ($scope.current_user) {
                        UserService.checkFollowing($scope.project.token).success(function(data) {
                            $scope.is_followed = data['data'];
                            $scope.ready = true;
                        });
                    } else {
                        $scope.ready = true;
                    }
                }).error(function(data) {
                
                });
            }
            if (mode == "edit" || mode =="new") {
                $scope.getCategories();
            
                if (mode=="new")
                    UserService.getTmpImgs().success(function(data) {
                        $scope.tmp_imgs = data['tmpimgs'];
                    });
                
                $scope.ready = true;
            }
        }

        $scope.redirectUserStory = function(person_token, person_slug) {
            $location.path("/user/"+person_token+"/"+person_slug);
        }

        $scope.getCategories = function() {
            UtilService.getCategories().success(function(data) {
                $scope.categories = data["data"];
            });
        }

        $scope.getCountriesCurrencies = function() {
            UtilService.getCountriesCurrencies().success(function(data) {
                $scope.countries = data['countries'];
                $scope.currencies = data['currencies'];
            });
        }

        $scope.donate = function() {
            UtilService.getCsrf().success(function() {
                $scope.handler.open({
                    name: "Donate!",
                    description: "project: " + $scope.countries[$scope.project.person.country_origin].symbol + $scope.donation.project + "  |  PolyGrant: " + $scope.countries[$scope.project.person.country_origin].symbol + $scope.donation.site,
                    amount: $scope.donation.amount * 100,
                });
            });
        }

        $scope.resetUpload = function() {
            $scope.uploader.flow.cancel();
            $scope.uploader_imagehash = {};
            $scope.images_new = [];
        }

        $scope.resetProject = function() {
            $scope.project = {
                name: "",
                description: "",
                desired_amount: "",
                retired: false,
                icon_url: "",
                project_url: "",
                video_url: "",
                token: "",
                companies: [],
                money_description: "",
                images: [],
                person: {tax_deductions_policy: "", currency: "", country_origin:""},
                goals: [],
                state: "",
                comments: []
            };
            $scope.new_goals = [];
            $scope.resetUpload();    
        }

        $scope.uploadSuccess = function(file, message) {
            var data = JSON.parse(message);
            
            delete $scope.errors_image[file.name];
            $scope.images_new.push(data.filename);
            $scope.uploader_imagehash[file.name] = data.filename;
        }

        $scope.isUploadValid = function(file) {
            if (file.size > 4009000) {
//                $scope.errors_image[file.name] = 'File size is too large.';
                return false;
            }
            if (['jpg', 'png', 'jpeg'].indexOf(file.getExtension()) < 0) {
//                $scope.errors_image[file.name] = 'File extension is invalid';
                return false;
            }

            if($scope.uploader.flow.files.length > 20) {
                return false;
            }

            return true;
        }

        $scope.uploadError = function(file, message) {
            message = JSON.parse(message);
            $scope.errors_image[file.name] = message['error'];
        }

        $scope.doUpload = function() {
            UtilService.getCsrfValue().success(function(data) {
                $scope.uploader.flow.opts.headers = {'x-csrf-token': data['csrf'], 
                                                     'Authorization': 'Bearer ' + SessionService.getToken()
                };
                $scope.uploader.flow.upload();
            });
        }

        $scope.deleteUpload = function(file, filename) {
            
            // Check if uploaded file failed... If it did then the image was never uploaded, so just delete it on screen.
            if (file && $scope.errors_image[file.name]) {
                var index = $scope.uploader.flow.files.indexOf(file);
                $scope.uploader.flow.files.splice(index, index+1);
                delete $scope.errors_image[file.name];
                return;
            }

            UtilService.getCsrf().success(function() {
                if (file) {
                    filename = $scope.uploader_imagehash[filename];
                }
                ProjectService.deleteUpload(filename, $scope.project.token).success(function() {
                    if (file) {
                        file.cancel();
                        var index = $scope.images_new.indexOf(filename);
                        $scope.images_new.splice(index, index+1);
                        delete $scope.uploader_imagehash[filename];
                    }
                    else {
                        var index = $scope.project.images.indexOf(filename);
                        $scope.project.images.splice(index, index+1);

                        // delete tmp imgs
                        var indextmp = $scope.tmp_imgs.indexOf(filename);
                        $scope.tmp_imgs.splice(indextmp, indextmp+1);
                    }
                });
            });
        }

        $scope.submit = function() {
            UtilService.getCsrf().success(function() {
                $scope.project.goals = $scope.existing_goals;

                ProjectService.submitProject($scope.project, $scope.images_new, $scope.new_goals).success(function(data) {
                    $scope.success = true;
                    $scope.errors = [];
                    if ($scope.mode == 'new') {
                        data = data['data'];
                        $location.path('projects/view/' + data.token + "/" + data.slug);
                    }
                    
                    if ($scope.mode == 'edit') {
                        $scope.new_goals = [];

                        // reload project data
                        ProjectService.getProject($scope.project.token).success(function(data) {
                            $scope.project = data['data'];
                            $scope.existing_goals = angular.copy(data['data'].goals);

                            // temporarily set disabled to true/false depending on state
                            $scope.project.retired = $scope.project.state == 'retired' ? true : false;
                            
                            // temporarily set free license to true if they've applied but have not yet been approved
                            $scope.project.is_free_license = $scope.project.is_free_license || $scope.project.has_applied_free_license;
                        });


                        $scope.project.images = $scope.project.images.concat($scope.images_new);
                        $scope.resetUpload();
                    }

                    // scroll back to top
                    $('html, body').animate( { scrollTop: 0}, 300);

                }).error(function(error) {
                    $scope.success = false;
                    $scope.errors = error.errors || ["Something isn't right. Please check your values"];  

                    // scroll back to top
                    $('html, body').animate( { scrollTop: 0}, 300);
                });
            });
        }

}]);

controllers.controller('MainCtrl', ['$scope', '$stateParams', '$location', '$timeout', 'UtilService', 'UserService', 'SessionService', 'SeoService',
    function($scope, $stateParams, $location, $timeout, UtilService, UserService, SessionService, SeoService) {
        $scope.current_user = SessionService.getIsAuthenticated();
        $scope.ready = false;
        $scope.date = new Date();
        
        $scope.categories = [];
        $scope.projects = [];
        $scope.countries = {};
        $scope.currencies = {};

        // pagination
        $scope.pageCurrent = 1;
        $scope.pageMaxItems = 1;
        $scope.pageDisplayItems = 30;

        // filter
        $scope.filter = "";
        $scope.filterCategory = "";
        $scope.filterFreeWorks = false;
        $scope.filterTaxExempt = false;

        SeoService.setData("PolyGrant Projects", "Search projects on PolyGrant");

        $scope.filterTaxExemptChange = function() {
            $scope.filterTaxExempt = !$scope.filterTaxExempt;
            if ($scope.filterTaxExempt == false) {
                $location.search('te', null);
            } else {
                $location.search('te', $scope.filterTaxExempt.toString());
            }
            $location.path('projects');
        }

        $scope.filterFreeWorksChange = function() {
            $scope.filterFreeWorks = !$scope.filterFreeWorks;
            if ($scope.filterFreeWorks == false) {
                $location.search('fcw', null);
            } else {
                $location.search('fcw', $scope.filterFreeWorks.toString());
            }
            $location.path('projects');
        }

        $scope.filterChange = function() {
            $location.search('filter', $scope.filter);
            $location.path('projects');
        }
         
        $scope.filterCategoryChange = function() {
            $location.search('category', $scope.filterCategory);
        }

        $scope.getCountriesCurrencies = function() {
            UtilService.getCountriesCurrencies().success(function(data) {
                $scope.countries = data['countries'];
                $scope.currencies = data['currencies'];
            });
        }

        $scope.viewProject = function(token, slug) {
            $location.url($location.path());
            $location.path("/projects/view/"+token+"/"+slug);
        }

        $scope.queryData = function() {
            UtilService.getCategories().success(function(data) {
                $scope.categories = data['data'];
            });
            UtilService.getProjects({"page": $stateParams.page, "category": $scope.filterCategory, "filter": $scope.filter, "fcw": $stateParams.fcw, "te": $stateParams.te}).success(function(data) {
                $scope.projects = data['data'];
                $scope.pageMaxItems = parseInt(data["total"]);
                $scope.ready = true;
            });
        }

        $scope.init = function() {
            $scope.filter = $stateParams.filter || "randomize";
            $scope.filterCategory = $stateParams.category || "";
            $scope.filterFreeWorks = $stateParams.fcw === "true";
            $scope.filterTaxExempt = $stateParams.te === "true";
            var page = parseInt($stateParams.page) || 1;
            $scope.pageMaxItems = page * $scope.pageDisplayItems;
            $scope.pageCurrent = page;
            $scope.getCountriesCurrencies();
        }


        $scope.pageChanged = function() {
            $location.search('page', $scope.pageCurrent);
            $location.path('/projects');
        }

        $scope.init();
        $scope.queryData();
}]);

controllers.controller('UserCtrl', ['$scope', '$location', '$stateParams', 'UserService', 'SessionService', 'UtilService',
    function($scope, $location, $stateParams, UserService, SessionService, UtilService) {
        $scope.current_user = SessionService.getIsAuthenticated();
        $scope.success = false;
        $scope.errors = [];
        $scope.show_spinner = false;
        $scope.gRecaptchaResponse = '';

        $scope.$watch('gRecaptchaResponse', function() {
            $scope.expired = false;
        });

        $scope.expiredCallback = function expiredCallback() {
            $scope.expired = true;
        }

        $scope.forgot = function(email) {
            UtilService.getCsrf().success(function() {
                $scope.show_spinner = true;
                UserService.forgot(email, $scope.gRecaptchaResponse).success(function(data) {
                    $scope.success = true;
                    $scope.show_spinner = false;
                    $scope.errors = [];
                }).error(function(errors) {
                    $scope.show_spinner = false;
                    $scope.errors = errors['errors'] || ['Something went wrong!'];
                    $scope.success = false;
                });
            });
        }

        $scope.reset = function(password) {
            UtilService.getCsrf().success(function() {
                $scope.show_spinner = true;
                UserService.reset($stateParams.token, password).success(function(data) {
                    $scope.success = true;
                    $scope.errors = [];
                    $scope.show_spinner = false;
                }).error(function(errors) {
                    $scope.errors = errors['errors'] || ['Something went wrong!'];
                    $scope.success = false;
                    $scope.show_spinner = false;
                });
            });
        }

        $scope.login = function(email, password) {
            UtilService.getCsrf().success(function() {
                $scope.show_spinner = true;
                UserService.login(email, password).success(function(data) {
                    SessionService.setToken(data['token']);
                    SessionService.setUserInfo(data['data']);
                    $location.path("/profile"); 
                }).error(function(errors) {
                    $scope.show_spinner = false;
                    $scope.errors = errors['errors'] || ['Invalid Credentials!'];
                });
            });
        }

        $scope.register = function(name, email, password, key) {
            UtilService.getCsrf().success(function() {
                $scope.show_spinner = true;
                UserService.register(name, email, password, key, $scope.gRecaptchaResponse).success(function(data) {
                    $location.path('/login');
                }).error(function(errors) {
                    $scope.show_spinner = false;
                    $scope.errors = errors['errors'] || ['Something wrong with the inputs!'];
                });
            });
        }

}]);

controllers.controller('LogoutCtrl', ['$scope', '$location', 'UserService', 'SessionService',
    function($scope, $location, UserService, SessionService) {

        SessionService.clearToken();
        SessionService.clearUserInfo();
        $location.path('/');
}]);

controllers.controller('ContactCtrl', ['$scope', '$location', 'ContactService', 'UtilService',
    function($scope, $location, ContactService, UtilService) {
        $scope.contact = {
            topic: "",
            details: "",
            your_email: ""    
        };
        $scope.success = false;
        $scope.errors = [];
        $scope.show_spinner = false;

        $scope.gRecaptchaResponse = '';

        $scope.$watch('gRecaptchaResponse', function() {
            $scope.expired = false;
        });

        $scope.expiredCallback = function expiredCallback() {
            $scope.expired = true;
        }


        $scope.submitContact = function() {
            $scope.show_spinner = true;
            UtilService.getCsrf().success(function() {
                ContactService.submitContact($scope.contact).success(function() {
                    $scope.errors = [];
                    $scope.success = true;  
                    $scope.contact = { topic: "", details: "", your_email: "" };
                    $scope.show_spinner = false;
                }).error(function(err) {
                    $scope.errors = err['errors'];  
                    $scope.success = false;
                    $scope.show_spinner = false;
                });
            });
        }
}]);

controllers.controller('StaticCtrl', ['$scope', '$location', 'SessionService',
    function($scope, $location, SessionService) {
        $scope.current_user = SessionService.getIsAuthenticated();
}]);
