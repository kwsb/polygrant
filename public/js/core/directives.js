
// Hides the Funding Goals tab in mobile on project view
directives.directive('hideGoals', [ function() {
    return {
        restrict: 'A',
        require: 'tabset',

        link: function(scope, element, attrs) {
            $(window).resize(function() {
                var gt = $('.goals-tab');
                // Switch to first tab if not in mobile
                if (gt.css("display") === "none") {
                    scope.$apply(function() {
                        scope.active_tabs[0] = true;
                        scope.active_tabs[1] = false;
                        scope.active_tabs[2] = false;
                        scope.active_tabs[3] = false;
                        scope.active_tabs[4] = false;
                    });
                }
            });
        }
    }
}]);


// Enforce 16:9 aspect ratio (currently used for carousel)
directives.directive('enforceAspectRatio', ['$window', '$timeout', function($window, $timeout) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var w = angular.element($window);
            w.bind('resize', function() {
                scope.$apply(update);
            });
            update();
            function update() {
                $timeout(function() {
                    var width = element.parent().width();
                    var height = (width * 9)/16.0;
    
                    $(".carousel").animate({ height: height});
                    //$('.carousel').css({'height': height + "px"});
                    //$('.carousel').css({'max-height': height + "px"});
                }, 500);
            };
        }
    }
}]);

directives.directive('holderjs', function() {
    return {
        link: function(scope, element, attrs) {
            attrs.$set('data-src', attrs.holderjs);
            Holder.run({images:element[0]});
        }
    }
});

directives.directive("donation-stripe", function() {
    return {
        link: function(scope, element, attrs) {
            $(window).on('popstate', function() {
                scope.$apply(function() {
                    scope.handler.close();
                });
            });
        }
    }
});

directives.directive("searchbar", ['$location', function($location) {
    return {
        link: function(scope, element, attrs) {

            element.bind("keydown keypress", function(event) {
                if(event.which == 13) {
                    scope.$apply(function() {
                        var searchterms = element.val();
                        $location.search({"terms": searchterms, "page": 1}).path("/search");
                    });
                    event.preventDefault();
                }
            });
        }
    }
}]);

directives.directive("slider", ['$timeout', function($timeout) {
    return {
        restrict: 'E',
        scope: {
                    model: '=ngModel',
                    change: '=ngChange'

        },
        template: '<div class="progress-box">\
                    <div class="progress-bar progress-bar-slider">\
                        <input class="progress-slider" type="range" min="1" max="100" data-ng-change="change" data-ng-model="model">\
                        <div class="inner" ng-style="{ width: model + \'%\' }"></div>\
                    </div>\
                   </div>\
                  ',
    }
}]);

/*
directives.directive("mypagination", function() {
    return {
        restrict: 'E',
        templateUrl: "partials/pagination.html",
        scope: { current_page: "=current_page", max_page: "=max_page" },
        link: function(scope, element, attrs) {
             
        }
    }
});
*/
