services.factory('SeoService', [
    function() {
        return {
            setData: function(title, description) {
                if (title)
                    document.title = title;
                if (description)
                    $("meta[name=description]").prop('content', description);

            }
        }
}]);

services.factory('CommentService', ['$http', 
    function($http) {
        return {
            createComment: function(project_token, description) {
                return $http.post('/api/comment/create', {'project_token': project_token, 'description': description});
            },

            editComment: function(comment_id, description) {
                return $http.post('/api/comment/edit', { 'comment_id': comment_id, 'description': description });
            },

            deleteComment: function(comment_id) {
                return $http.post("/api/comment/delete", { 'comment_id': comment_id });
            }
        }
}]);


services.factory('AdminService', ['$http',
    function($http) {
        return {
            serviceUser: function(id, service) {
                return $http.post("/api/admin/"+service+"/users", {'id': id});
            },

            serviceProject: function(id, service) {
                return $http.post("/api/admin/"+service+"/projects", {'id': id});
            },

            getUsers: function(service) {
                return $http.get("/api/admin/"+service+"/users");
            },

            getProjects: function(service) {
                return $http.get("/api/admin/"+service+"/projects");
            }
        }
}]);

services.factory('ReceiptService', ['$http',
    function($http) {
        var donation_token = null;

        return {
            setDonationToken: function(value) {
                donation_token = value;
            },
            getDonationToken: function() {
                return donation_token;
            }
        }
}]);

services.factory('StripeService', ['$http',
    function($http) {
        return {
            setupConnect: function(code) {
                return $http.post("/api/stripe/connect/", {"code": code, "hi": "hi" });
            },
            
            charge: function(project_token, donation, stripe_token) {
                return $http.post("/api/stripe/charge", {"project_token": project_token, "donation": donation, "stripe_token": stripe_token});
            },
        }
}]);

services.factory('ProjectService', ['$http',
    function($http) {
        return {
            launchProject: function(project_token) {
                return $http.post("/api/projects/launch", { "project_token": project_token });
            },

            submitNews: function(project_token, id, description) {
                return $http.post("/api/projects/newsupdate", {"project_token": project_token, "id":id, "description": description});
            },

            deleteNews: function(id) {
                return $http.post("/api/projects/newsupdate/delete", {"id": id});
            },

            submitFollow: function(project_token) {
                return $http.post("/api/projects/follow/" + project_token);
            },

            getProject: function(token) {
                return $http.get("/api/projects/view/" + token );
            },

            submitProject: function(project, images_new, new_goals) {
                return $http.post("/api/projects/submit", {"project": project, 'images_new': images_new, 'new_goals': new_goals});
            },

            validateProject: function(project) {
                return $http.post("/api/projects/validate", {"project": project});
            },

            deleteUpload: function(filename, project_token) {
                return $http.post("/api/projects/upload/delete", {"project_token": project_token, "filename": filename});
            },

            deleteExistingGoal: function(goal_token) {
                return $http.post("/api/projects/goals/delete", {"goal_token": goal_token});
            },
        }
}]);

services.factory('UtilService', ['$http', 
    function($http) {
        return {
            getCountriesCurrencies: function() {
                return $http.get('/api/countries_currencies');
            },
            
            getDonation: function(token) {
                return $http.get("/api/donation/"+token);
            },
            getCategories: function() {
                return $http.get("/api/categories");
            },

            getProjects: function(data) {
                return $http.get("/api/projects", {params: data});
            },

            searchProjects: function(data) {
                return $http.get("/api/search", {params: data });
            },

            getCsrf: function() {
                return $http.get("/api/csrf").success(function(data) {
                    $http.defaults.headers.post['x-csrf-token'] = data['csrf'];
                });
            },

            getCsrfValue: function() {
                return $http.get("/api/csrf");
            }
        }
}]);


services.factory('SessionService', ['localStorageService', 
    function(localStorageService) {
        return {
            getIsAdmin: function() {
                if (localStorageService.get('token')) {
                    return localStorageService.get('userinfo')['is_admin'] == true ? true : false
                }
                return false;
            },

            getIsAuthenticated: function() {
                if (localStorageService.get('token')) {
                    return true;
                }
                return false;
            },

            setUserInfo: function(val) {
                return localStorageService.set('userinfo', val);
            },

            getUserInfo: function() {
                return localStorageService.get('userinfo');
            },

            clearUserInfo: function() {
                return localStorageService.remove('userinfo');
            },

            setToken: function(val) {
                return localStorageService.set('token', val);
            },

            getToken: function() {
                return localStorageService.get('token');
            },

            clearToken: function() {
                return localStorageService.remove('token');
            }
        }
}]);

services.factory('TokenInterceptor', ['$q', '$window', '$location', 'SessionService', function($q, $window, $location, SessionService) {
    return {
        request: function(config) {
            config.headers = config.headers || {};
            if (SessionService.getToken()) {
                config.headers.Authorization = 'Bearer ' + SessionService.getToken();
            }
            return config;
        },
        
        requestError: function(rejection) {
            return $q.reject(rejection);
        },

        response: function(response) {
            return response || $q.when(response);
        },

        responseError: function(rejection) {
            if(rejection != null && rejection.status === 401 && SessionService.getIsAuthenticated()) {
                SessionService.setToken(null);
                $location.path("/login");
            }

            return $q.reject(rejection);
        }
    };
}]);


services.factory('ContactService', ['$http', 
    function($http) {
        return {

            submitContact: function(contact) {
                return $http.post("/api/contact",  {'contact': contact});
            }
        }
}]);

services.factory('UserService', ['$http',
    function($http) { 
        return {
            getTmpImgs: function() {
                return $http.get("/api/user/tmpimgs");
            },

            getPublicProfile: function(person_token) {
                return $http.get("/api/user/public/profile/" + person_token);
            },

            checkFollowing: function(project_token) {
                return $http.get("/api/user/profile/check/following/"+project_token);
            },

            reset: function(token, password) {
                return $http.post("/api/user/reset", {'token': token, 'password': password});
            },

            forgot: function(email, gRecaptchaResponse) {
                return $http.post("/api/user/forgot", {'email': email, 'g-recaptcha-response': gRecaptchaResponse });
            },

            login: function(email, password) {
                return $http.post("/api/user/login", {'email': email, 'password': password});
            },
       
            register: function(name, email, password, key, gRecaptchaResponse) {
                return $http.post("/api/user/register", {'name': name, 'email': email, 'password': password, 'key': key, 'g-recaptcha-response': gRecaptchaResponse });
            },

            updateProfile: function(fields) {
                return $http.post("/api/user/profile", {'fields': fields});  
            },

            getProfile: function() {
                return $http.get("/api/user/profile");
            },
        }
}]);
