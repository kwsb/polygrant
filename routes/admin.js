var models = require("../models");

var bodyParser = require('body-parser');
var jsonParser = bodyParser.json();

var csrf = require('csurf');
var csrfProtection = csrf();

var router = require('express').Router();
var jwt = require('jsonwebtoken');
var expressJwt = require('express-jwt');
var Sequelize = require('sequelize');
var Promise = require('bluebird');
var utils = require('../utils/utils.js');
var emailer = require('../utils/email');

var config = require('../config');

var secret = config.jwt_secret;

router.get("/:service/projects", expressJwt({ secret: secret }), function(req, res) {
    var user_token = req.user.token;
    var service = req.params.service;
    
    var where_query = utils.getProjectWhereQuery(service);

    models.person.find({where: { token: user_token }}).then(function(person) {
        return models.project.findAll({where: where_query}).then(function(projects) {
            if (!person) {
                res.status(404).json({});
                return;
            }

            if (!person.is_admin) {
                res.status(403).json({});
                return;
            }

            res.json({"projects": projects});
        });
    }).catch(function(error) {
        res.status(500).json({});
    });
});

router.post("/:service/projects", jsonParser, csrfProtection, expressJwt({ secret: secret }), function(req, res) {
    var user_token = req.user.token;
    var service = req.params.service;
    var id = req.body.id;

    var where_query = utils.getProjectWhereQuery(service);
    var update_query = utils.getProjectUpdateQuery(service);

    models.person.find({where: { token: user_token }}).then(function(person) {
        return models.project.find({where: { id: id }}).then(function(project) {

            if (!person) {
                res.status(404).json({});
                return;
            }

            if (!person.is_admin) {
                res.status(403).json({});
                return;
            }

            if (!project) {
                res.status(404).json({});
                return;
            }

            return project.updateAttributes(update_query).then(function(project) {
                res.json({'project': project});
            });
        });
    }).catch(function(error) {
        res.status(500).json({});
    });
});

router.get("/:service/users", expressJwt({ secret: secret }), function(req, res) {
    var user_token = req.user.token;
    var service = req.params.service;

    var where_query = utils.getUserWhereQuery(service);

    models.person.find({where: { token: user_token }}).then(function(person) {
        return models.person.findAll({ where: where_query }).then(function(users) {
            if (!person) {
                res.status(404).json({});
                return;
            }

            if (!person.is_admin) {      
                res.status(403).json({});
                return;
            }

            res.json({"users": users});
        });
    }).catch(function(error) {
        res.status(500).json({});
    });
});

router.post("/:service/users", jsonParser, csrfProtection, expressJwt({ secret: secret }), function(req, res) {
    var user_token = req.user.token;
    var service = req.params.service;
    var id = req.body.id;

    var where_query = utils.getUserWhereQuery(service);
    var update_query = utils.getUserUpdateQuery(service);

    models.person.find({where: { token: user_token }}).then(function(person) {
        return models.person.find({where: { id: id }}).then(function(person_target) {

            if (!person) {
                res.status(404).json({});
                return;
            }

            if (!person.is_admin) {
                res.status(403).json({});
                return;
            }

            if (!person_target) {
                res.status(404).json({});
                return;
            }

            return person_target.updateAttributes(update_query).then(function(person_target) {
                res.json({'user': person_target});
            });
        });
    }).catch(function(error) {
        res.status(500).json({});
    });
});

module.exports = router;
