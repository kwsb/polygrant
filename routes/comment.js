var bodyParser = require('body-parser');
var jsonParser = bodyParser.json();

var models = require("../models");

var csrf = require('csurf');
var csrfProtection = csrf();

var config = require("../config");
var router = require('express').Router();
var jwt = require('jsonwebtoken');
var expressJwt = require('express-jwt');
var Sequelize = require('sequelize');
var env = config.env || "development";
var db_config = require("../db_config.json")[env];
var sequelize = new Sequelize(db_config.database, db_config.username, db_config.password, db_config);

var utils = require("../utils/utils");
var uuid = require("node-uuid");

var logger = require('../utils/logger.js');
var messages = require('../utils/messages.js');
var secret = config.jwt_secret;

var recaptcha = require('express-recaptcha');
recaptcha.init(config.recaptcha_pk, config.recaptcha_sk);

// user creates an account
router.post("/create", jsonParser, csrfProtection, expressJwt({secret: secret}), recaptcha.middleware.verify, function(req, res) {

    var log_id = uuid.v4();
    req.log = logger.log.child({ log_id: log_id});
    req.log.info({req: req}, messages.log_begin_route('/user/regsiter'));

    var project_token = req.body.project_token;
    var person_id = req.user.id; 
    var description = req.body.description;
    
    models.project.find({ where: { token: project_token } }).then(function(project) {
        if (!project) {
            res.status(404).json();
            return;   
        }

        // deny comments if project is not in launched state
        if(project.state != "launched") {
            res.status(400).json();
            return;
        }

        return models.comment.create({ description: description, projectId: project.id, personId: person_id }).then(function(comment) {

            // append some person data since we can't eager load it in create()
            comment.dataValues.person = {};
            comment.dataValues.person.name = req.user.name;
            comment.dataValues.person.token = req.user.token;

            res.json({'comment': comment});
        });
    }).catch(function(error) {
        console.log(error.stack);
        if (error.name =="SequelizeValidationError") {
            var errors = error.errors.map(function(x) { return x.message });
            res.status(400).json({'errors': errors});
        } else {
            req.log.warn({err: error}, messages.log_db_general_err());
            res.status(500).json({});
        }
    });
});

router.post("/edit", jsonParser, csrfProtection, expressJwt({secret: secret}), function(req, res) {
    var log_id = uuid.v4();
    req.log = logger.log.child({ log_id: log_id});
    req.log.info({req: req}, messages.log_begin_route('/user/regsiter'));

    var comment_id = req.body.comment_id;
    var person_id = req.user.id;
    var description = req.body.description;
    
    var comment_model = null;

    models.comment.find({where: { id: comment_id }, include:[{model: models.person}, {model: models.project}]}).then(function(comment) {
        if (!comment) {
            res.status(404).json();
            return;
        }

        if (comment.personId != person_id) {
            res.status(403).json();
            return;
        }

        // just return if nothing has changed
        if (comment.description == description) {
            res.json();
            return;
        }

        return sequelize.transaction(function(t) {
            return comment.updateAttributes({ description: description}, {transaction: t}).then(function(comment) {
                comment_model = comment;
            });
        }).then(function() {
            res.json();
        });
        
    }).catch(function(error) {
        console.log(error.stack);
        if (error.name =="SequelizeValidationError") {
            var errors = error.errors.map(function(x) { return x.message });
            res.status(400).json({'errors': errors});
        } else {
            req.log.warn({err: error}, messages.log_db_general_err());
            res.status(500).json({});
        }
    });
});


router.post("/delete", jsonParser, csrfProtection, expressJwt({secret: secret}), function(req, res) {
    var log_id = uuid.v4();
    req.log = logger.log.child({ log_id: log_id});
    req.log.info({req: req}, messages.log_begin_route('/user/regsiter'));

    var comment_id = req.body.comment_id;
    var person_id = req.user.id;


    models.comment.find({where: { id: comment_id }, include:[{model: models.person}, {model: models.project}]}).then(function(comment) {
        if (!comment) {
            res.status(404).json();
            return;
        }

        if (comment.personId != person_id) {
            res.status(403).json();
            return;
        }

        return sequelize.transaction(function(t) {
            return comment.destroy({transaction: t}).then(function() {
                req.log.info({req: req}, messages.log_success('comment'));
                res.json({});
            });
        });

    }).catch(function(error) {
        console.log(error.stack);
        req.log.warn({err:error}, messages.log_db_general_err());
        res.status(500).json({});
    });
});


module.exports = router;
