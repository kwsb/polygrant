var models = require("../models");

var bodyParser = require('body-parser');
var jsonParser = bodyParser.json();

var csrf = require('csurf');
var csrfProtection = csrf();
var validator = require('validator');

var router = require('express').Router();
var jwt = require('jsonwebtoken');
var expressJwt = require('express-jwt');
var Sequelize = require('sequelize');

var utils = require("../utils/utils");
var uuid = require("node-uuid");
var logger = require('../utils/logger.js');
var messages = require('../utils/messages.js');


var crypto = require('crypto');

var config = require('../config');
var path = require('path');

var countries = require('country-data').countries;
var currencies = require('currencies');

var secret = config.jwt_secret;

var emailer = require('../utils/email');



router.get("/csrf", csrfProtection, function(req, res) {
    res.json({"csrf": req.csrfToken()});
});

router.post('/contact', jsonParser, csrfProtection, function(req, res) {
    var topic = validator.toString(req.body.contact.topic);
    var details = validator.toString(req.body.contact.details);
    var your_email = validator.toString(req.body.contact.your_email);

    // sanitize user input
    is_email = validator.isEmail(your_email);
    if (!is_email) {
        res.status(500).json({'errors': ['Please enter a valid email!']});
        return;
    }

 
    if (config.contact_options.indexOf(topic) < 0) {
        res.status(500).json({"errors": ["Invalid topic!"]});
        return;
    }
    
    emailer.sendEmailText({
        'from': your_email,
        'to': config.email_support,
        'subject': "[SUPPORT] " + topic,
        "text": details
    }).then(function(result) {
        res.json({});
    }).catch(function(err) {
        var log_id = uuid.v4();
        req.log = logger.log.child({ log_id: log_id});
        req.log.error({err: err}, messages.log_app_err('NODEMAILER FAILED'));

        res.status(500).json({'errors': ["Hm... Something went wrong on our end! If the problem persists, you can contact us directly at " + config.email_support]});
    });

});

router.get('/countries_currencies', function(req, res) {

    var names = {};
    var currencieslist = config.supported_currencies.map(function(x) { return { 'code': x }; });
    config.supported_countries.forEach(function(x) {
        var curr = countries[x].currencies[0];
        names[x] = {'code': x, 'name':countries[x].name, 'currency': curr, 'money_limit': config.money_limits[x], 'symbol': currencies.get(curr).symbol};    
    });
    res.json({"countries": names, 'currencies': currencieslist });
});

router.get("/categories", function(req, res) {
    models.category.findAll({ order: 'name ASC' }).then(function(categories) {
        res.json({"data": categories });
    });
});

router.get("/projects", function(req, res) {

    var log_id = uuid.v4();
    req.log = logger.log.child({ log_id: log_id});
    req.log.info({req: req}, messages.log_begin_route('/projects'));

    var page = parseInt(req.query.page) - 1;
    var filter = req.query.filter;
    var category = req.query.category;

    // free cultural works checked
    var fcw = req.query.fcw == 'true';
    // tax exemption checked
    var te = req.query.te == 'true';

    var order = filter == "randomize" ? [[Sequelize.fn('random')]] : null;
    order = filter == "newest" ? [["createdAt", "ASC"]] : order;
    
    var include_category = {model: models.category, attributes: ['name']};
    if (category)
        include_category = {model: models.category, attributes: ['name'], where: {name: category}};

    var where_query = { state: "launched" };
    if (fcw) 
        where_query = Sequelize.and({ is_free_license: true }, where_query);

    var include_person = { model: models.person, attributes:['name', 'tax_exempt', 'country_origin', 'city_state']};
    if (te)
        include_person = { model: models.person, attributes:['name', 'tax_exempt', 'country_origin', 'city_state'], where: { tax_exempt: true}};

    models.project.findAndCountAll({order: order, limit: 30, offset: page * 30, include: [
        include_person,
        include_category
    ], where: where_query }).then(function(result) {

        req.log.info({req: req}, messages.log_success("query"));
        
        res.json({"data": result.rows, "total": result.count});
    }).catch(function(error) {
        req.log.warn({err: error}, messages.log_db_general_err());
        res.status(500).json({});  
    });
});

router.get("/donation/:token", expressJwt({secret: secret, credentialsRequired: false}), function(req, res) {
    var token = req.params.token;
    
    // reject null token
    if (token == null) {
        res.status(404).json({"error": ["Invalid Input"]});
        return;
    }

    models.donation.find({where: { token: token }, include: [{model: models.project, attributes:['name'], include: [{ model: models.person, attributes: ['country_origin']}]}]}).then(function(donation) {
        if(!donation) {
            res.status(404).json({});
            return;
        }

        // deny querying for other people's receipts 
        if ((!req.user && donation.personId) || (req.user && donation.personId != req.user.id)) {
            res.status(401).json({});
            return;
        }

        donation = donation.toJSON();
        delete donation.personId;
        delete donation.projectId;
        delete donation.id;
        delete donation.updatedAt;

        res.json({'donation': donation});

    }).catch(function(error) {
        res.status(500).json({});  
    });
});

module.exports = router;
