var config = require('../config');

var bodyParser = require('body-parser');
var jsonParser = bodyParser.json();
var busboy = require('connect-busboy');
var busboyParser = busboy({
    limits: {
        fileSize: config.upload_filesize, 
        files: 1,
    }
});
var fs = require('fs');
var uuid = require('node-uuid');
var sanitize = require('sanitize-filename');
var csrf = require('csurf');
var csrfProtection = csrf();

var models = require("../models");
var Sequelize = require("sequelize");
var env = config.env || "development";
var db_config = require("../db_config.json")[env];
var sequelize = new Sequelize(db_config.database, db_config.username, db_config.password, db_config);
var Promise = require("bluebird");

var path = require('path');
var router = require('express').Router();
var jwt = require('jsonwebtoken');
var expressJwt = require('express-jwt');

var sharp = require('sharp');

var utils = require('../utils/utils.js');
var logger = require('../utils/logger.js');
var messages = require('../utils/messages.js');
var emailer = require('../utils/email');

var secret = config.jwt_secret;

// View a project page
router.get('/view/:token', expressJwt({secret: secret, credentialsRequired: false}), function(req, res) {
    var token = req.params.token;    
   
    var log_id = uuid.v4();
    req.log = logger.log.child({ log_id: log_id});
    req.log.info({req: req}, messages.log_begin_route('/projects/view/'+ token));   

    models.project.find({where: { token: token} , order: [[models.newsupdate, 'createdAt','DESC'], [models.goal, 'amount', 'ASC'], [models.comment, 'createdAt', 'DESC']], include: [
        { model: models.newsupdate, limit: 20, attributes: ['id', 'description', 'createdAt']},
        { model: models.person, attributes: ['slug', 'currency', 'tax_deductions_policy', 'country_origin', 'name', 'stripe_connected', 'token', 'tax_deductions', 'organization_type', 'tax_exempt', 'email_public', 'city_state']},
        { model: models.category, attributes: ['name'] },
        { model: models.goal, attributes: ['name', 'description', 'amount', 'token']},
        { model: models.comment, attributes:['description', 'id', 'createdAt'], include: [{model: models.person, attributes:['name', 'token']}] },
    ]}).then(function(project) {
        if(project) {

            // Ensure only admin or creator can access unapproved projects
            var auth_user = req.user;
            if (project.state != "launched" && project.state != "retired") {

                if (!auth_user) {
                    res.status(404).json({});
                    return;
                } 
                else if (project.person.token != auth_user.token && !auth_user.is_admin) {
                    res.status(404).json({});
                    return;
                }
            }

            return utils.getCurrentProjectDonations(project).then(function(donations) {
                req.log.info({req: req}, messages.log_success('query')); 
                res.json({'data': project, 'donations': donations});
            });
        } else {
            req.log.warn({req: req}, messages.log_db_not_found('project'));
            res.status(404).json({});
        }
    }).catch(function(error) {
        console.log(error.stack);
        req.log.warn({err: error}, messages.log_db_general_err());   
        res.status(500).json({});
    });
});

// delete a news update
router.post("/newsupdate/delete", jsonParser, csrfProtection, expressJwt({secret: secret}), function(req, res) {
    var news_id = req.body.id;
    var user = req.user;

    var log_id = uuid.v4();
    req.log = logger.log.child({ log_id: log_id});
    req.log.info({req: req}, messages.log_begin_route('/projects/newsupdate/delete'));

    models.newsupdate.find({where: { id: news_id }, include:[
        { model: models.project, include: [{ model: models.person }] }
    ]}).then(function(newsupdate) {
      
        // deny disabled person 
        if(newsupdate.project.person.isDisabled()) {
            req.log.warn({req: req}, messages.log_user_disabled());
            res.status(403).json({});
            return;
        }
 
        // could not find newsupdate
        if (!newsupdate) {
            req.log.warn({req: req}, messages.log_db_not_found('newsupdate'));
            res.status(404).json({});
            return;
        }

        // check editing user can edit this newsupdate
        if (newsupdate.project.person.id != user.id) {
            req.log.warn({req: req}, messages.log_unauthorized('project'));
            res.status(403).json({});
            return;
        }

        return sequelize.transaction(function(t) {
            return newsupdate.destroy({transaction: t}).then(function() {
                req.log.info({req: req}, messages.log_success('newsupdate'));
                res.json();
            });
        });
    });
});

// create or update a newsupdate
router.post("/newsupdate", jsonParser, csrfProtection, expressJwt({secret: secret}), function(req, res) {
    var log_id = uuid.v4();
    req.log = logger.log.child({ log_id: log_id});
    req.log.info({req: req}, messages.log_begin_route('/projects/newsupdate'));   

    var person_id = req.user.id;
    var project_token = req.body.project_token;
    var description = req.body.description;

    // if id is present, do updating
    var news_id = req.body.id;
    
    var start_year = new Date(new Date().getFullYear(), 0, 1);
    var end_year = new Date(new Date().getFullYear(), 11, 31);  
   
    models.person.find({where: { id: person_id }}).then(function(person) {
        return models.project.find({where: { token: project_token }, include: [
            { model: models.newsupdate, attributes: ['id', 'description', 'createdAt']},
            { model: models.person, attributes: ['id', 'name', 'stripe_connected', 'token', 'tax_deductions', 'organization_type']},
            { model: models.category, attributes: ['name'] }
        ]}).then(function(project_row) {
       
            if(person.isDisabled()) {
                req.log.warn({req: req}, messages.log_user_disabled());
                res.status(403).json({});
                return;
            }

            // project exists and project belongs to editing user
            if(project_row && project_row.person.id == person_id) {

                // update news
                if (news_id) {

                    return models.newsupdate.find({where: { id: news_id }, include:[{model: models.project}]}).then(function(newsupdate) {
                        if (!newsupdate) {
                            req.log.warn({req: req}, messages.log_db_not_found('newsupdate'));
                            res.status(404).json({});
                            return;
                        }
                        return sequelize.transaction(function(t) {
                            return newsupdate.updateAttributes({ description: description }, {transaction: t}).then(function(newsupdate) {
                                req.log.info({req: req}, messages.log_success('update'));   
                                res.json({"newsupdate": newsupdate});
                            });
                        });
                    });
                // create news 
                } else {
                    return models.newsupdate.create({description: description, projectId: project_row.id, personId: person_id}).then(function(newsupdate) {
                        return models.newsupdate.findAll({where: { projectId: project_row.id }, limit: 20, order: [['createdAt','DESC']], attributes: ['id', 'description', 'createdAt']}).then(function(newsupdates) {
                            req.log.info({req: req}, messages.log_success('create'));   
                            res.json({'project': project_row, 'newsupdates': newsupdates});
                        });
                    });
                }

            // project exists but does not belong to editing user
            } else if (project_row && project_row.person.id != person_id) {
                req.log.warn({req: req}, messages.log_unauthorized('project'));
                res.status(403).json({'errors': ['You are not authorized to edit this project']});
            } else if (!project_row) {
                req.log.warn({req: req}, messages.log_db_not_found('project'));   
                res.status(404).json({});
            } else {
                req.log.warn({req: req}, messages.log_general_err());   
                res.status(400).json({});
            }
        });
    }).catch(function(error) {
        console.log(error.stack); 
        if (error.name =="SequelizeValidationError") {
            var errors = error.errors.map(function(x) { return x.message });
            res.status(400).json({'errors': errors});
        } else {
            req.log.warn({err: error}, messages.log_db_general_err());   
            res.status(500).json({});
        }
    });
});

// Follow or unfollow a project
router.post("/follow/:token", jsonParser, csrfProtection, expressJwt({secret: secret}), function(req, res) {
    var person_token = req.user.token;
    var project_token = req.params.token;

    var log_id = uuid.v4();
    req.log = logger.log.child({ log_id: log_id});
    req.log.info({req: req}, messages.log_begin_route('/projects/follow/'+ project_token));

    models.person.find({where: { token: person_token }, include:[{model: models.project, as: 'followings'}] }).then(function(person) {
        return models.project.find({where: { token: project_token }, include:[{model: models.person, as: 'followings'}] }).then(function(project) {
            if (!person || !project) {
                req.log.warn({req: req}, messages.log_db_not_found('person or project'));
                res.status(404).json({});
                return;
            }
            return person.hasFollowing(project).then(function(result) {
                if (result) {
                    // unfollow
                    return person.removeFollowing(project).then(function() {
                        req.log.info({req: req}, messages.log_success('unfollow'));
                        res.json({}); 
                    });
                } else {
                    // follow
                    return person.addFollowing(project).then(function() {
                        req.log.info({req: req}, messages.log_success('follow'));
                        res.json({});  
                    });
                }
            });
        });
    }).catch(function(error) {
        req.log.warn({err: error}, messages.log_db_general_err());
        res.status(500).json({});
    });
});

// deletes an uploaded file
router.post("/upload/delete", jsonParser, csrfProtection, expressJwt({secret: secret}), function(req, res) {
    var person_token = req.user.token;
    var person_id = req.user.id;
    var project_token = req.body.project_token;
    var image_filename = req.body.filename;

    if (!image_filename) {
        res.status(500).json({});
        return;
    }

    image_filename = sanitize(image_filename);

    var log_id = uuid.v4();
    req.log = logger.log.child({ log_id: log_id});
    req.log.info({req: req}, messages.log_begin_route('/upload/delete for project '+ project_token));


    models.person.find({where: { id: person_id }}).then(function(person) {
        // Delete from existing project
        if (project_token) {
            return models.project.find({where: { token: project_token }, include:[{ model: models.person}]}).then(function(project) {

                if(person.isDisabled()) {
                    req.log.warn({req: req}, messages.log_user_disabled());
                    res.status(400).json({});
                    return;
                }

                // project exists and project belongs to editing user
                if(project && project.person.id == person_id) {
                    
                    // remove file - try both uploads/ + tmp/
                    var filepath = utils.getUploadDir(project.token, image_filename);
                    utils.deleteFile(filepath);
                    var filepath_tmp = utils.getTmpDir(image_filename, person_token);
                    utils.deleteFile(filepath_tmp);
                    // now remove from db
                    var filtered = project.images.filter(function(x) { return x != image_filename; });
                        
                    return project.updateAttributes({ images: filtered }).then(function(project) {
                        if(!project) {
                            req.log.warn({req: req}, messages.log_db_not_found('project'));
                            res.status(404).json();
                            return;
                        }

                        res.json();
                    });
                } else {
                    req.log.warn({req: req}, messages.log_unauthorized('upload'));
                    res.status(403).json({});
                    return;
                }
            });

        // Delete from new project
        } else {
            var filepath = utils.getTmpDir(image_filename, person_token);
            utils.deleteFile(filepath);
            res.json();
        }
    }).catch(function(error) {
        req.log.warn({err:error}, messages.log_db_general_err());
        res.status(500).json({});
    });
});

router.post('/upload', busboyParser, csrfProtection, expressJwt({secret: secret}), function(req, res) {
    var person_token = req.user.token;
    var error = "";
    var filename_uuid = "";

    if (req.busboy) {

        // stream to file to disk
        req.busboy.on('file', function(fieldname, file, filename, encoding, mimetype) {
            file.on('limit', function(data) {
                error = "File '" + filename + "' filesize is too big!";
            });
     
            // grab file extension + check against valid extensions
            var ext = path.extname(filename);
            if (utils.validFileExtension(ext) == true) {
                filename_uuid = uuid.v4() + ext;
                var saveTo = utils.getTmpDir(filename_uuid, person_token);

                utils.createPath(path.dirname(saveTo), function(err) {
                    if (err) {
                        error = "Could not upload!";
                            console.log(err);
                        file.resume();
                    } else {
                        var tmpdir = utils.getTmpFolder(person_token);
                        // Check to see if # of files is not over max_files
                        fs.readdir(tmpdir, function(err, files) {

                            if (files && files.length >= config.max_files) {
                                error = "Reached max of "+config.max_files+" images. Please delete a few.";
                                file.resume();
                            } else {

                                // This might help with speedup if there are problems
                                 /*
                                    var transformer = sharp()
                                        .rotate()
                                        .resize(750, 422)
                                        .toFile(saveTo, function(err) {
                                            console.log(err);
                                        });
                                        file.pipe(transformer);
                                */

                                // use this for now
                                var transformer = sharp()
                                    .rotate()
                                    .resize(750, null);
                                file.pipe(transformer).pipe(fs.createWriteStream(saveTo));
                            }
                        });
                    }
                });
            } else {
                error = "File '" + filename + "' does not have a valid file extension";
                file.resume();
            }
        });

        // not used... but can be used for accessing any fields passed
        req.busboy.on('field', function(fieldname, val, fieldnameTruncated, valTruncated) {
            console.log('Field [' + fieldname + ']: value: ' + val);
        });

        // Return response to client
        req.busboy.on('finish', function() {
            if (error)
                res.status(500).json({"error": error});
            else 
                res.json({"filename": filename_uuid});
        });


        req.busboy.on('error', function() {
            console.log("ERROR");
            error = "ERROR";
        });
        req.pipe(req.busboy);
    }
});

// deletes an existing goal
router.post("/goals/delete", jsonParser, csrfProtection, expressJwt({secret: secret}), function(req, res) {
    var goal_token = req.body.goal_token;
    var user = req.user;



    var log_id = uuid.v4();
    req.log = logger.log.child({ log_id: log_id});
    req.log.info({req: req}, messages.log_begin_route('/projects/goal/delete/'+ goal_token));

    models.goal.find({where: { token: goal_token }, include: [
        { model: models.project, include: [
            { model: models.person }
        ]}
    ]}).then(function(goal) {

        if (!goal) {
            req.log.warn({req: req}, messages.log_db_not_found('person'));
            res.status(404).json({});
            return;
        }

        // check if person is disabled
        if(goal.project.person.isDisabled()) {
            req.log.warn({req: req}, messages.log_user_disabled());
            res.status(400).json({});
            return;
        }


        // Check if goal belongs to editing user
        if (goal.project.person.token != user.token) {
            req.log.warn({req: req}, messages.log_unauthorized('goal delete'));
            res.status(403).json({});
            return;
        }

        return sequelize.transaction(function(t) {
            return goal.destroy({transaction: t}).then(function() {
                req.log.info({req: req}, messages.log_success('goal'));
                res.json({});
            });
        });

    }).catch(function(error) {
        req.log.warn({err:error}, messages.log_db_general_err());
        res.status(500).json({});
    });
});

// launches a project
router.post("/launch", jsonParser, csrfProtection, expressJwt({secret: secret}), function(req, res) {
    var project_token = req.body.project_token;
    var person_id = req.user.id;

    var log_id = uuid.v4();
    req.log = logger.log.child({ log_id: log_id});
    req.log.info({req: req}, messages.log_begin_route('/projects/launch'+ project_token));

    models.project.find({where: { token: project_token }, include:[{model: models.person}] }).then(function(project_row) {
        if (!project_row) {
            req.log.warn({req: req}, messages.log_db_not_found('launch'));
            res.status(404).json({});
            return;
        }

        if(project_row.person.isDisabled()) {
            req.log.warn({req: req}, messages.log_user_disabled());
            res.status(400).json({});
            return;
        }

        // project exists and project belongs to editing user
        if (project_row.person.id != person_id) {
            req.log.warn({req: req}, messages.log_unauthorized('launch'));
            res.status(403).json({});
            return;
        }

       return project_row.updateAttributes({ state: 'pending approval' }).then(function(project) {
            if (!project) {
                res.status(500).json({});
                return;
            }


            // If user has applied for free license
            if (project_row.has_applied_free_license && project_row.license.length > 0) {
                emailer.sendEmailText({
                    to: config.email_support,
                    from: project_row.person.email,
                    subject: "[FREE CULTURE REQUEST] " + project_row.name,
                    text:
                    "# " + project_row.token +
                        "\nproject name: " + project_row.name +
                        "\nemail: " + project.person.email +
                        "\nLicense: " + project_row.license +
                        "\n\nURL: " + "https://polygrant.com/projects/view/"+ project_row.token + "/" + project_row.slug
                }).catch(function(err) {
                    req.log.error({err: err}, messages.log_app_err('NODEMAILER FAILED - sending INTERNAL Free Culture update email'));
                });
             }

            
            var email_public = project.person.email_public || "";
            // send an email about new project:
            emailer.sendEmailText({
                to: config.email_support,
                from: project_row.person.email,
                subject: "[NEW PROJECT] " + project_row.name,
                text:
                "# " + project_row.token +
                    "\nproject name: " + project_row.name +
                    "\nproject blurb: " + project_row.blurb +
                    "\nproject funding: " + project_row.desired_funding +
                    "\nemail: " + project_row.person.email +
                    "\npublic email: " + email_public +
                    "\ncountry: " + project_row.person.country_origin +
                    "\n\nURL: " + "https://polygrant.com/projects/view/"+ project_row.token + "/" + project_row.slug
            }).catch(function(err) {
                req.log.error({err: err}, messages.log_app_err('NODEMAILER FAILED - sending INTERNAL new project email'));
            });



            req.log.info({req: req}, messages.log_success('launch')); 
            res.json({'project_state': project.state});
        });
    }).catch(function(error) {
        console.log(error.stack);
        req.log.warn({err:error}, messages.log_db_general_err());
        res.status(500).json({});
    });
});

// saves or creates a project
router.post('/submit', jsonParser, csrfProtection, expressJwt({secret: secret}), function(req, res) {
    var project = req.body.project;
    var person_id = req.user.id;
    var person_name = req.user.name;
    var person_token = req.user.token;
    var person_email = req.user.email;

    var log_id = uuid.v4();
    req.log = logger.log.child({ log_id: log_id});
    req.log.info({req: req}, messages.log_begin_route('/projects/follow/'+ project.token));


    var images = project.images.map(function(x) { return sanitize(x); });
    var images_new = req.body.images_new.map(function(x) { return sanitize(x); });
    var images_all = images.concat(images_new);

    var new_goals = req.body.new_goals;
    var old_goals = req.body.project.goals;

    // Check to make sure all image links are HTTPS (optional?) or using localhost (required)
    var invalid_images = utils.verifyMarkdownImageLinks(project.description);
    if (invalid_images['hosts'].length > 0) {
        var msgs = "";
        for (var i=0; i< invalid_images['hosts'].length; i++) {
            msgs = msgs + "\n --- " + invalid_images['hosts'][i];
        }
        res.status(400).json({'errors': ["Please only use images that you've uploaded. (On image thumbnails, Right-click on \"Link\" -> Copy link address). \nThe following image links are not valid:"  + msgs]}); 
        return;
    }


    if (project.video_url) {
        var valid_video_link = utils.verifyVideoURL(project.video_url);
        if (!valid_video_link) {
            res.status(400).json({"errors": ["Your Project Video is invalid. Please ensure it is an embed link from Youtube or Vimeo"]});
            return;
        }
    }
    
    models.category.find({where: { id: project.categoryId }}).then(function(category) {
        return models.person.find({where: { id: person_id }}).then(function(person) {

            // For email ... get the new/updated project model from transaction
            var project_model = null;

            if(person.isDisabled()) {
                req.log.warn({req: req}, messages.log_user_disabled());
                res.status(400).json({});
                return;
            }

            if (!category) {
                req.log.warn({req: req}, messages.log_db_not_found('category'));
                res.status(404).json({'errors':["invalid category"]});
                return;
            }

            // update existing project
            if (project.token) {
                return models.project.find({where: { token: project.token }, include:[{model: models.person}, {model: models.goal, include: [{model: models.project}]}] }).then(function(project_row) {
                    return utils.getCurrentProjectDonations(project).then(function(donations) {
                        // project exists and project belongs to editing user
                        if(project_row && project_row.person.id == person_id) {

                            // DENY updates to project certain fields IF donations have been made:
                            //  fields: [name]
                            var project_name = project.name;
                            var donation_sum = donations[0].dataValues.donation_sum || 0;
                            if (donation_sum > 0 && project.name != project_row.name) {
                                project_name = project_row.name; 
                            }

                            //var retire_date = project_row.retire_date;
                            if (project.retired == true) {
                                project.state = "retired";
                                retire_date = sequelize.fn('NOW');
                            }
                            // Project came out of retirement
                            else if (project.retired == false && project_row.state == 'retired') {
                                project.state = 'prelaunch';
                                retire_date = null;
                            }
                            else {
                                project.state = project_row.state;
                                retire_date = null;
                            }

                            // reset free_license if creator has changed it back to 'No'
                            var is_free_license = project.is_free_license == false ? false : project_row.is_free_license;
                            // throw out free license if license field is empty
                            is_free_license = project.is_free_license && !project.license ? false : is_free_license;

                            
                            // Convert all new links from /uploads_tmp to /uploads
                            project.description = utils.convertImageLinks(project.description, project.token);

                            return sequelize.transaction(function(t) {
                                return project_row.updateAttributes({
                                    retire_date: retire_date,
                                    name: project_name,
                                    description: project.description,
                                    project_url: project.project_url,
                                    icon_url: project.icon_url,
                                    video_url: project.video_url,
                                    desired_funding: project.desired_funding,
                                    categoryId: category.id,
                                    money_description: project.money_description,
                                    companies: project.companies,
                                    license: project.license,
                                    blurb: project.blurb,
                                    has_applied_free_license: project.is_free_license,
                                    images: images_all,
                                    is_free_license: is_free_license,
                                    state: project.state
                                }, {transaction: t}).then(function(project) {

                                    if(project) {
                                        project_model = project;

                                        // Create new goals
                                        return Promise.map(new_goals, function(goal) {
                                            return models.goal.create({projectId: project.id, name:goal.name, description: goal.description, amount: goal.amount }, { transaction: t});
                                        }).then(function(result) {
                               
                                            // Update goals
                                            return Promise.map(project.goals, function(goal) {
                                                // Grab data from client for this goal
                                                var g = old_goals.filter(function(x) { return x.token == goal.token; })[0];
                                                if (g) {
                                                    // DENY updates to certain goal fields IF donations have been made:
                                                    // fields: everything!

                                                    if(donation_sum >= goal.amount && (goal.name != g.name || goal.description != g.description || goal.amount != g.amount)) {
                                                        return;
                                                    }
                                                    return goal.updateAttributes({ name: g.name, description: g.description, amount: g.amount }, {transaction: t}); 
                                                }
                                            });
                                        });

                                    } else {
                                        req.log.warn({req: req}, messages.log_failed('update'));
                                        res.status(400).json({});
                                    }   
                                });
                            }).then(function(result) {

                                // If user has applied for free license
                                if (!project_row.is_free_license && project.is_free_license && project.license.length > 0) {

                                    emailer.sendEmailText({
                                        to: config.email_support,
                                        from: person_email,
                                        subject: "[FREE CULTURE REQUEST] " + project_model.name,
                                        text: 
                                        "# " + project_row.token +
                                            "\nproject name: " + project_model.name + 
                                            "\nemail: " + person_email +
                                            "\nLicense: " + project_model.license + 
                                            "\n\nURL: " + "https://polygrant.com/projects/view/"+ project_model.token + "/" + project_model.slug 
                                    }).catch(function(err) {
                                        req.log.error({err: err}, messages.log_app_err('NODEMAILER FAILED - sending INTERNAL Free Culture update email'));
                                    });
                                 }
                                


                                // remove old images
                                var current_images = project_model.images;
                                var diff = current_images.filter(function(i) { return images_all.indexOf(i) < 0;});
                                for (var i = 0; i < diff.length; i++) {
                                    var filepath = utils.getUploadDir(project_row.token, diff[i]);
                                    utils.deleteFile(filepath);
                                }

                                // Save to ES
//                                utils.updateOrCreateElasticProject(project, category.name, person);


                                // move images from tmp to upload
                                for(var i = 0; i<images_new.length; i++) {
                                    utils.moveFile(project_row.token, images_new[i], person_token);
                                }
                             
                                req.log.info({req: req}, messages.log_success('update'));
                                res.json({});

                            });
                        } else if (project_row && project_row.person.id != person_id) {
                            req.log.warn({req: req}, messages.log_unauthorized('project'));
                            res.status(403).json({'errors': ['You are not authorized to edit this project']});
                        } else if (!project_row) {
                            req.log.info({req: req}, messages.log_db_not_found('project'));
                            res.status(404).json({});
                        } else {
                            req.log.warn({req: req}, messages.log_general_err());
                            res.status(400).json({});
                        }
                    });
                });

            // create new project
            } else {

                return sequelize.transaction(function(t) {
                    return models.project.create({
                        name: project.name,
                        description: project.description,
                        project_url: project.project_url,
                        icon_url: project.icon_url,
                        video_url: project.video_url,
                        desired_funding: project.desired_funding || -1,
                        categoryId: category.id,
                        money_description: project.money_description,
                        companies: project.companies,
                        license: project.license,
                        personId: person_id,
                        blurb: project.blurb,
                        has_applied_free_license: project.is_free_license,
                        images: images_all,
                    }, { transaction: t}).then(function(project) {
                        if(project) {

                            // Convert all new links from /uploads_tmp to /uploads
                            project.description = utils.convertImageLinks(project.description, project.token);
                            return project.updateAttributes({description: project.description}, { transaction: t}).then(function(project) {

                                project_model = project;

                                // Save to ES
        //                        utils.updateOrCreateElasticProject(project, category.name, person);
                                       
                                return Promise.map(new_goals, function(goal) {
                                    return models.goal.create({projectId: project.id, name:goal.name, description: goal.description, amount: goal.amount }, { transaction: t}); 
                                });
                                
                            });                                    
                        } else {    
                            req.log.warn({req: req}, messages.log_failed('create'));
                            res.status(500).json({});
                        }
                    });
                }).then(function(result) {

                    // move images from tmp to upload
                    for(var i = 0; i<images_new.length; i++) {
                        utils.moveFile(project_model.token, images_new[i], person_token);
                    }


                    req.log.info({req: req}, messages.log_success('create'));
                    res.json({data: {'token': project_model.token, 'slug': project_model.slug} }); 
                });
            }
        });
    }).catch(function(error) {
        console.log(error.stack);
        if (error.name =="SequelizeValidationError") {
            var errors = error.errors.map(function(x) { return x.message });
            res.status(400).json({'errors': errors});  
        } else {
            req.log.warn({err: error}, messages.log_db_general_err());
            res.status(500).json({});
        }
    });
});


module.exports = router;
