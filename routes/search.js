var models = require("../models");
var Sequelize = require("sequelize");

var router = require('express').Router();
var jwt = require('jsonwebtoken');
var expressJwt = require('express-jwt');

var utils = require('../utils/utils.js');
var uuid = require('node-uuid');
var logger = require('../utils/logger.js');
var messages = require('../utils/messages.js');
var config = require("../config");

var secret = config.jwt_secret;

router.get('/', expressJwt({secret: secret, credentialsRequired: false}), function(req, res) {

    var log_id = uuid.v4();
    req.log = logger.log.child({ log_id: log_id});
    req.log.info({req: req}, messages.log_begin_route('/search'));

    var searchterm = req.query.terms;
    var page = parseInt(req.query.page) - 1;
    var filter = req.query.filter;
    var category = req.query.category;

    // free cultural works checked
    var fcw = req.query.fcw == 'true';
    // tax exemption checked
    var te = req.query.te == 'true';

    var order = filter == "randomize" ? [[Sequelize.fn('random')]] : null;
    order = filter == "newest" ? [["createdAt", "ASC"]] : order;

    var include_category = {model: models.category, attributes: ['name']};
    if (category)
        include_category = {model: models.category, attributes: ['name'], where: {name: category}};


    // NOTE: Sequelize 2.0.6 cannot include OR searches on associations:
    // https://github.com/sequelize/sequelize/issues/3527
    // So, cannot add person fields to where, ie: { 'person.name': { ilike: ..... } }
    // temp solution: use 'sequelize.where(sequelize.col(.....`
    var where_query = { state: "launched" };
    where_query = 
        Sequelize.and(where_query, 

            Sequelize.or(
                { name: { ilike: '%'+searchterm+'%'} },
                { description: { ilike: '%'+searchterm+'%'} },
                { blurb: { ilike: '%'+searchterm+'%'} },
                { license: { ilike: '%'+searchterm+'%'} },
                Sequelize.where(Sequelize.col('person.name'), 'ILIKE', '%'+searchterm+'%'),
                Sequelize.where(Sequelize.col('person.country_origin'), 'ILIKE', '%'+searchterm+'%')
            )
        );
    if (fcw)
        where_query = Sequelize.and({ is_free_license: true }, where_query);

    var include_person = { model: models.person, attributes:['name', 'tax_exempt', 'country_origin', 'city_state']};
    if (te)
        include_person = { model: models.person, attributes:['name', 'tax_exempt', 'country_origin', 'city_state'], where: { tax_exempt: true } };



    models.project.findAll({ order: order, limit: 30, offset: page * 30, include: [
        include_category,
        include_person
    ], where: where_query  }).then(function(results) {

        req.log.info({req: req}, messages.log_success("query"));

        res.json({"data": results, "total": results.length});
    }).catch(function(error) {
        console.log(error);
        req.log.info({err: error}, messages.log_db_general_err());
        res.status(500).json({});
    });
});



// This uses Elastic Search... but requires too much ram! Query the DB for now.
/*
router.get('/', expressJwt({secret: secret, credentialsRequired: false}), function(req, res) {
    var data = req.query;
    var searchterm = req.query.terms;
    var page = parseInt(req.query.page) - 1;
    var filter = req.query.filter;
    var category = req.query.category;     

    // free cultural works checked
    var fcw = req.query.fcw == 'true';
    // tax exemption checked
    var te = req.query.te == 'true';

    var sort = [];
    var must = [];
    if (category) {
        must.push({ query: { query_string: { query: category, fields:["category"] } } });
    }

    if (fcw) {
        must.push({ term: { is_free_license: true } });
    }

    if (te) {
        must.push({ term: { person_tax_exempt: true } });
    }

    if (sort == "newest") {
        sort.push({ "createdAt": { "order": "asc" } });
    }


    elastic.es_client.search({
        index: 'horme',
        type: 'project',
        size: 30, 
        from: page * 30,

        body: {
            sort: sort,
            query: {
                filtered: {
                    filter: {
                        bool: {
                            should: [{
                                query: { 
                                    query_string: { 
                                        query: "*"+searchterm+"*",
                                        fields: ["license", "name", "category", "description", "person_name"],
                                    }
                                }
                            }],
                            must: must,
                            must_not: [
                                { term: { retired: true } },
                                { term: { disabled: true } },
                                { term: { is_approved: false } }, 
                            ]
                        }
                    }
                }
            }
        }
    }, function(err, eres) {
        if (!err) {
            if(res)
                res.json({"data": eres.hits.hits, "total": eres.hits.total});
            else
                res.json({});
        } else
            res.status(500).json({"error": "error"});
    });
});
*/


module.exports = router;
