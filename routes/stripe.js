// Removed as part of release

// Stripe connect
router.post('/connect', jsonParser, csrfProtection, expressJwt({secret: secret}), function(req, res) {
});


router.post("/charge", jsonParser, csrfProtection, expressJwt({secret: secret, credentialsRequired: false}), function(req, res) {
});

module.exports = router;
