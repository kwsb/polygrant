var bodyParser = require('body-parser');
var jsonParser = bodyParser.json();

var models = require("../models");

var csrf = require('csurf');
var csrfProtection = csrf();


var config = require("../config");
var router = require('express').Router();
var jwt = require('jsonwebtoken');
var expressJwt = require('express-jwt');
var Sequelize = require('sequelize');
var Promise = require('bluebird');
var env = config.env || "development";
var db_config = require("../db_config.json")[env];
var sequelize = new Sequelize(db_config.database, db_config.username, db_config.password, db_config);

//var elastic = require("../utils/elastic");

var utils = require("../utils/utils");
var emailer = require("../utils/email");

var countries = require('country-data').countries;
var uuid = require('node-uuid');
var logger = require('../utils/logger.js');
var messages = require('../utils/messages.js');
var secret = config.jwt_secret;

var fs = require('fs');
var crypto = require('crypto');

var recaptcha = require('express-recaptcha');
recaptcha.init(config.recaptcha_pk, config.recaptcha_sk);

router.get("/public/profile/:token", function(req, res) {
    var person_token = req.params.token;

    var log_id = uuid.v4();
    req.log = logger.log.child({ log_id: log_id});
    req.log.info({req: req}, messages.log_begin_route('/user/public/profile'));

    models.person.find({where: { token: person_token }, include: [{model: models.project, attributes:['name', 'createdAt', 'blurb', 'token', 'slug', 'retire_date'], required:false, where: Sequelize.or({state: 'launched'}, {state: 'retired'}) }], attributes: ['name', 'email_public', 'bio'] }).then(function(person) {
        if (!person) {
            req.log.warn({req: req}, messages.log_db_not_found('person'));
            res.status(404).json({});
            return;
        }

        res.json({'person': person});
    }).catch(function(error) {
        console.log(error.stack);
        res.status(500).json({});
    });
});

// Get user's tmp images (used for loading images still in user's tmp folder when creating new project)
router.get("/tmpimgs", expressJwt({ secret: secret }), function(req, res) {
    var person_token = req.user.token;
    var tmpdir = utils.getTmpFolder(person_token);

    fs.readdir(tmpdir, function(err, list) {
        if(err)  {
            res.json({"tmpimgs": []});
            return;
        }
        req.log.info({req: req}, messages.log_success('query'));
        res.json({"tmpimgs": list});
    });
});


// Get user's profile
router.get("/profile", expressJwt({ secret: secret }), function(req, res) {

    var log_id = uuid.v4();
    req.log = logger.log.child({ log_id: log_id});
    req.log.info({req: req}, messages.log_begin_route('/user/profile'));

    var user_token = req.user.token;
    models.person.find({where: { token: user_token }, order:[[models.project, 'name', 'ASC']], include: [
        {model: models.donation, attributes: ['amount', 'createdAt', 'currency'], include: [
            {model: models.project, attributes:['name', 'slug', 'token'], include: [{model: models.person, attributes:['country_origin']}]}
        ]},
        {model: models.project, as: 'followings', through: {attributes:[]}, attributes:['name', 'slug', 'token']},
        {model: models.project, attributes:['id', 'token', 'name', 'slug', 'state', 'createdAt', 'is_free_license']}
    ]}).then(function(person) {
        if (!person) {
            req.log.warn({req: req}, messages.log_db_not_found('person'));
            res.status(404).json({});
            return;
        } else {
            var projects_stats = {};

            // sequelize 2.0.3 has bug where you cannot eager load and call fn(SUM()), so use this workaround instead:
            // -----------------------------------------------------------------
            // Get statistics for creators
            return new Promise.map(person.projects, function(project) {
                return models.donation.sum('amount', {where: { projectId: project.id}}).then(function(total) {
                    return models.donation.count('*', {where: { projectId: project.id}}).then(function(count) {
                            var token = project.token;
                            projects_stats[token] = {"name": project.name, "slug": project.slug, "token": project.token, "total": total, "count": count};
                    });
                });
            }).then(function() {
                req.log.info({req: req}, messages.log_success('query'));

                person = utils.removePersonTokenData(person.toJSON());
                res.json({"data": person, "projects_stats": projects_stats});
            });
            // --------------------------------------------------------------------

        }

    }).catch(function(error) {
        res.status(500).json({});
    });
});


// check if a user is following a project
router.get("/profile/check/following/:token", expressJwt({ secret: secret }), function(req, res) {

    var project_token = req.params.token;
    var user_token = req.user.token;
    
    var log_id = uuid.v4();
    req.log = logger.log.child({ log_id: log_id});
    req.log.info({req: req}, messages.log_begin_route('/user/profile/check/following/'+project_token));


    models.person.find({where: { token: user_token }, include:[{model: models.project, as: 'followings'}] }).then(function(person) {
        return models.project.find({where: { token: project_token }, include:[{model: models.person, as: 'followings'}] }).then(function(project) {
            if (!person || !project) {
                req.log.warn({req: req}, messages.log_db_not_found('person or project'));
                res.status(404).json({});
                return;
            }
 
            return person.hasFollowing(project).then(function(result) {
                req.log.info({req: req}, messages.log_success('query'));
                res.json({"data": result});
            });
        });
    }).catch(function(error) {
        req.log.warn({err: error}, messages.log_db_general_err());
        res.status(500).json({});
    });
});


// update profile
router.post("/profile", jsonParser, csrfProtection, expressJwt({ secret: secret }), function(req, res) {

    var log_id = uuid.v4();
    req.log = logger.log.child({ log_id: log_id});
    req.log.info({req: req}, messages.log_begin_route('/user/profile'));

    var email = req.body.fields.email || "";
    var email_public = req.body.fields.email_public;
    // set public email to null if user wants to remove it
    if (!email_public) {
        email_public = null;
    } else {
        email_public = email_public.toLowerCase();
    }

    var current_password = req.body.fields.current_password;
    var password = req.body.fields.password;

    var tax_deductions = req.body.fields.tax_deductions;
    var tax_exempt = req.body.fields.tax_exempt;

    var country_origin = req.body.fields.country_origin || "";
    var city_state = req.body.fields.city_state || "";
    city_state = city_state.toLowerCase();

    var bio = req.body.fields.bio;

    var tax_deductions_policy = req.body.fields.tax_deductions_policy;
   
    var organization_type = req.body.fields.organization_type || "";

    var currency = countries[country_origin] ? countries[country_origin].currencies[0] : 'USD';

    models.person.find({where: { token: req.user.token }, order:[[models.project, 'name', 'ASC']], include: [
        {model: models.donation, attributes: ['amount', 'createdAt'], include: [
            {model: models.project, attributes:['name', 'slug', 'token']}
        ]},
        {model: models.project, as: 'followings', through: {attributes:[]}, attributes:['name', 'slug', 'token']},
        {model: models.project, attributes:['id', 'token', 'name', 'slug', 'state', 'createdAt', 'is_free_license'], include: [{ model: models.category, attributes:['name']}]}
    ]}).then(function(person) {

        if (!person) {
            req.log.warn({req: req}, messages.log_db_not_found('person'));
            res.status(404).json({});
            return;
        }

        // reset tax_exempt if they set it to NO
        var is_tax_exempt = tax_exempt == false ? false : person.tax_exempt;

        return person.verifyPassword(current_password).then(function() {
            // If new password was passed in
            if(password) {
                return sequelize.transaction(function(t) {
                    return person.set({ bio: bio, city_state: city_state, tax_deductions_policy: tax_deductions_policy, currency: currency, has_applied_tax_exempt: tax_exempt, email: email, email_public: email_public, tax_exempt: is_tax_exempt, country_origin: country_origin, tax_deductions: tax_deductions, organization_type: organization_type, password: password }).save({ transaction: t}).then(function(person) {

                        // This uses old ES
    //                    utils.updateProjectsFromProfile(person);  
                    

                        if (person.has_applied_tax_exempt && !person.tax_exempt) {
                            emailer.sendEmailText({
                                to: person.email,
                                from: config.email_support,
                                subject: "Tax Exemption Application",
                                text: "Hello,\n\nYou've applied for a Tax Exempt status on PolyGrant! We just need to verify some details to get started.\n\nPlease include any applicable details that we can use to verify your status. For those in the United States, this means providing your EIN, Legal Name (or DBA), City, State.\n\n\nPlease reply to this email with the relevant information, and we'll get started on the process!\n\n\n"
                            }).then(function() {
                                emailer.sendInternalEmail(
                                    "[TAX EXEMPT REQUEST] " + person.email, 
                                    "# " + person.token + 
                                        "\nemail: " + person.email +
                                        "\npublic email: " + person.email_public +  
                                        "\norganization type: " + person.organization_type + 
                                        "\ntax deductions: " + person.tax_deductions + 
                                        "\ntax deductions policy: " + person.tax_deductions_policy + 
                                        "\ncountry: " + person.country_origin + 
                                        "\ncity_state: " + person.city_state
                                ).catch(function(err) {
                                    req.log.error({err: err}, messages.log_app_err('NODEMAILER FAILED - sending INTERNAL Tax Exempt email'));
                                })
                            }).catch(function(err) {
                                req.log.error({err: err}, messages.log_app_err('NODEMAILER FAILED - sending Tax Exempt email'));
                            });
                        }

                        var person_json = utils.removePersonTokenData(person.toJSON());
                        var token = jwt.sign(person_json, secret, { expiresInMinutes: 1440 });

                        req.log.info({req: req}, messages.log_success('update'));
                        res.json({"data": person_json, "token":token});
                    });
                });
            } else {
                // otherwise just update other fields
                return sequelize.transaction(function(t) {
                    return person.updateAttributes({ bio: bio, city_state: city_state, tax_deductions_policy: tax_deductions_policy, currency: currency, has_applied_tax_exempt: tax_exempt, email: email, email_public: email_public, tax_exempt: is_tax_exempt, country_origin: country_origin, tax_deductions: tax_deductions, organization_type: organization_type }, { transaction: t }).then(function(person) {

                        // This uses old ES
    //                    utils.updateProjectsFromProfile(person);  


                        if (person.has_applied_tax_exempt && !person.tax_exempt) {
                            emailer.sendEmailText({
                                to: person.email,
                                from: config.email_support,
                                subject: "Tax Exemption Application",
                                text: "Hello,\n\nYou've applied for a Tax Exempt status on PolyGrant! We just need to verify some details to get started.\n\nPlease include any applicable details that we can use to verify your status. For those in the United States, this means providing your EIN, Legal Name (or DBA), City, State.\n\n\nPlease reply to this email with the relevant information, and we'll get started on the process!\n\n\n"
                            }).then(function() {
                                emailer.sendInternalEmail(
                                    "[TAX EXEMPT REQUEST] " + person.email,
                                    "# " + person.token +
                                        "\nemail: " + person.email +
                                        "\norganization type: " + person.organization_type +
                                        "\ntax deductions: " + person.tax_deductions +
                                        "\ntax deductions policy: " + person.tax_deductions_policy +
                                        "\ncountry: " + person.country_origin
                                ).catch(function(err) {
                                    req.log.error({err: err}, messages.log_app_err('NODEMAILER FAILED - sending INTERNAL Tax Exempt email'));
                                })
                            }).catch(function(err) {
                                req.log.error({err: err}, messages.log_app_err('NODEMAILER FAILED - sending Tax Exempt email'));
                            });
                        }

                        var person_json = utils.removePersonTokenData(person.toJSON());
                        var token = jwt.sign(person_json, secret, { expiresInMinutes: 1440 });
                            
                        req.log.info({req: req}, messages.log_success('update'));
                        res.json({"data": person_json, "token":token});
                    });
                });
            }
        });
    }).catch(function(error) {
        console.log(error.stack);
        if (error.name =="SequelizeValidationError") {
            var errors = error.errors.map(function(x) { return  x.message });
            res.status(400).json({'errors': errors});
        } else {
            req.log.warn({err: error}, messages.log_db_general_err());
            res.status(500).json({'errors': ['Invalid Credentials']});
        }
    });

});


// resets user password
router.post("/reset", jsonParser, csrfProtection, function(req, res) {

    var log_id = uuid.v4();
    req.log = logger.log.child({ log_id: log_id});
    req.log.info({req: req}, messages.log_begin_route('/user/reset'));

    var token = req.body.token;
    var password = req.body.password;
    models.person.find({where: { reset_token: token } }).then(function(person) {
        if (!person) {
            req.log.warn({req: req}, messages.log_db_not_found('person'));
            res.status(404).json({});
            return;
        }

        // Check expire time
        var diff = person.reset_expire - Date.now();
        if (diff <= 0) {
            req.log.warn({req: req}, messages.log_failed('reset'));
            res.status(500).json({'errors': ['This token has expired. Please request another.']});
            return;
        }

        return person.set({reset_token: null, reset_expire: null, password: password}).save().then(function(person) {
            if (!person) {
                req.log.warn({req: req}, messages.log_failed('reset'));
                res.status(500).json({});
                return;
            }

            res.json({});
        });
    }).catch(function(error) {
        if (error.name =="SequelizeValidationError") {
            var errors = error.errors.map(function(x) { return x.path + ": " + x.message });
            res.status(400).json({'errors': errors});
        } else {
            req.log.warn({err: error}, messages.log_db_general_err());
            res.status(500).json({});
        }
    });
});


// user forgot their password
router.post("/forgot", jsonParser, csrfProtection, recaptcha.middleware.verify, function(req, res) {

    var log_id = uuid.v4();
    req.log = logger.log.child({ log_id: log_id});
    req.log.info({req: req}, messages.log_begin_route('/user/forgot'));

    var email = req.body.email;

    if(req.recaptcha.error) {
        res.status(400).json({'errors': ['invalid recaptcha!']});
        return;
    }


    if (!email) {
        res.status(400).json({"errors":["Can't leave email blank"]});
        return;
    }

    models.person.find({ where: {'email': email} }).then(function(person) {
        if(!person) {
            req.log.warn({req: req}, messages.log_db_not_found('person'));
            res.status(404).json({});
            return;
        }

        // Generate key and expire time (1 hour)
        var token = crypto.randomBytes(64).toString('hex');
        var expire = new Date();
        expire.setHours(expire.getHours() + 1);

        return person.updateAttributes({reset_token: token, reset_expire: expire}).then(function(person) {
            if (!person) {
                req.log.warn({req: req}, messages.log_failed('update'));
                res.status(500).json({});
                return;   
            } else {
                var link = config.reset_link + token;
                emailer.sendEmailText({
                    'to': person.email,
                    'subject': "Password Reset Link",
                    'text': "A password reset has been initiated. If you did not request this, please ignore this message.\n\n" + link
                }).then(function() {
                    req.log.info({req: req}, messages.log_success('update'));
                    res.json({});
                }).catch(function(err) {
                    req.log.error({err: err}, messages.log_app_err('NODEMAILER FAILED'));
                    res.json({});
                });
            }
        }); 
    }).catch(function(error) {
        console.log(error.stack);
        if (error.name =="SequelizeValidationError") {
            var errors = error.errors.map(function(x) { return x.path + ": " + x.message });
            res.status(400).json({'errors': errors});
        } else {
            req.log.warn({err: error}, messages.log_db_general_err());
            res.status(500).json({});
        }
    });
});


// user logs in
router.post("/login", jsonParser, csrfProtection, function(req, res) {

    var log_id = uuid.v4();
    req.log = logger.log.child({ log_id: log_id});
    req.log.info({req: req}, messages.log_begin_route('/user/login'));

    var email = req.body.email;
    var password = req.body.password;

    // NOTE: add this if you want to also load projects user is following
    // include:[{model: models.project, through: {attributes:[]}, as: 'followed', attributes: ['token']}]
    models.person.find({ where: {'email': email} }).then(function(person) {
        if(!person) {
            req.log.warn({req: req}, messages.log_db_not_found('person'));
            // Just tell client something went wrong, but don't specify what
            res.status(400).json({});
            return;
        }
        return person.verifyPassword(password).then(function() {
            // Create a token
            person = utils.removePersonTokenData(person.toJSON());
            var token = jwt.sign(person, secret, { expiresInMinutes: 1440});
               
            req.log.info({req: req}, messages.log_success('logged in'));
            res.json({'token':token, 'data': person });
        });
    }).catch(function(error) {
        if (error.name =="SequelizeValidationError") {
            var errors = error.errors.map(function(x) { return x.path + ": " + x.message });
            res.status(400).json({'errors': errors});
        } else {
            req.log.warn({err: error}, messages.log_db_general_err());
            res.status(400).json({});
        }
    });
});


// user creates an account
router.post("/register", jsonParser, csrfProtection, recaptcha.middleware.verify, function(req, res) {

    var log_id = uuid.v4();
    req.log = logger.log.child({ log_id: log_id});
    req.log.info({req: req}, messages.log_begin_route('/user/regsiter'));

    var email = req.body.email || "";
    email = email.toLowerCase();

    var password = req.body.password || "";
    var name = req.body.name || "";

    if(req.recaptcha.error) {
        res.status(400).json({'errors': ['Invalid recaptcha!']});
        return;
    }


    models.person.create({ name: name, email: email, password: password}).then(function(person) {
        if(person) {
            emailer.sendEmailText({
                'to': person.email,
                'subject': 'Welcome!',
                'text': 'Thanks for signing up on PolyGrant!'
            }).catch(function(err) {
                req.log.error({err: err}, messages.log_app_err('NODEMAILER FAILED - register'));
            });
            emailer.sendInternalEmail('[NEW USER] ' + person.email, 'person_token: ' + person.token + '\nperson_email: ' + person.email).catch(function(err) {
                req.log.error({err: err}, messages.log_app_err('NODEMAILER FAILED - register sendInternalEmail()'));
            });
            req.log.info({req: req}, messages.log_success('create'));
            res.json({});
        } else {
            req.log.warn({req: req}, messages.log_failed('create'));
            res.status(500).json({});
        }
    }).catch(function(error) {
        console.log(error.stack);
        if (error.name =="SequelizeValidationError" || error.name == "SequelizeUniqueConstraintError") {
            var errors = error.errors.map(function(x) { return x.path + ": " + x.message });
            res.status(400).json({'errors': errors});
        } else {
            req.log.warn({err: error}, messages.log_db_general_err());
            res.status(500).json({});
        }
    });
});

module.exports = router;
