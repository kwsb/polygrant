var nodemailer = require('nodemailer');
var wellknown = require('nodemailer-wellknown');
var config = require('../config');
var Promise = require('bluebird');

var transporter = Promise.promisifyAll(
    nodemailer.createTransport({
        service: '',
        auth: {
            user: config.email_noreply,
            pass: config.email_noreply_password
        }
    })
);

module.exports = {

    sendEmailText: function(data) {

        var mailOptions = {
            from: data['from'] || config.email_noreply,
            to: data['to'],
            subject: data['subject'],
            text: data['text']
        };

        return transporter.sendMailAsync(mailOptions);
    },

    sendInternalEmail: function(subject, text) {
       
       var mailOptions = {
            from: config.email_noreply,
            to: config.email_info,
            subject: subject,
            text: text  
       };

        return transporter.sendMailAsync(mailOptions);
    }
};
