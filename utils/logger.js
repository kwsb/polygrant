var bunyan = require('bunyan');
exports.log = new bunyan.createLogger({ 
    name: 'polygrant', 
    serializers: bunyan.stdSerializers,
    streams: [
        {
            type: 'rotating-file',
            period: '1d',
            count: 7,
            path: './logs/access.log',
            level: 'trace'
        },
        {
            type: 'rotating-file',
            period: '1d',
            count: 7,
            path: './logs/error.log',
            level: 'error'
        }
    ],
});

