
module.exports = {

    // logging messages
    log_begin_route: function(link) {
        return ' requested ' + link;
    },

    log_db_not_found: function(something) {
        return ' could not find ' + something;
    },

    log_db_general_err: function() {
        return ' something went wrong in the database';
    },

    log_user_disabled: function() {
        return ' person is disabled; action denied';
    },

    log_unauthorized: function(something) {
        return 'person has unauthorized access to ' + something + ' ; action denied';
    },

    log_success: function(operation) {
        return operation + ' success!';
    },

    log_failed: function(operation) {
        return operation + ' failed!';
    },

    log_general_err: function() {
        return ' triggered general error';
    },

    log_fs_err: function(msg) {
        return msg;
    },

    log_app_err: function(msg) {
        return msg;
    },

};
