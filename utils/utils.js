var crypto = require('crypto');
//var elastic = require('../utils/elastic');

var models = require("../models");
var Sequelize = require('sequelize');
var fs = require('fs');
var os = require('os');
var path = require('path');
var sanitize = require('sanitize-filename');

var config = require('../config');
var logger = require('../utils/logger.js');
var messages = require('../utils/messages.js');
var uuid = require('node-uuid');

var url = require('url');

module.exports = {
    removePersonTokenData: function(person) {
        delete person.password;
        delete person.password_hash;
        delete person.stripe_publishable_key;
        delete person.stripe_access_token;
        delete person.stripe_refresh_token;
        delete person.reset_token;
        delete person.reset_expire;

        return person;
    },
    
    slugify: function(str) {
        var slug = require('slug');
        return slug(str);
    },

    genToken: function(bytes) {
        bytes = bytes || 20
        var token = crypto.randomBytes(bytes).toString('hex');
        return token;
    },

    calc_fee: function(project) {
        if (project.person.tax_exempt || project.is_free_license)
            return 0.0;
        return 0.05;
    },

    verifyVideoURL: function(video_url) {
        
        var link = url.parse(video_url);
        if (link.pathname && link.pathname != "/" && (link.host == "player.vimeo.com" || link.host == "www.youtube.com")) {
            return true;
        } else {
            return false;
        }
    },


    // Verify all markdown image links are pointing to this domain
    verifyMarkdownImageLinks: function(markdown_text) {

        var md = MarkdownIt = require('markdown-it'),
            iterator = require('markdown-it-for-inline'),
            md = new MarkdownIt();

        var valid_hosts = ['polygrant.com', 'www.polygrant.com'];
        var invalids = {'hosts': [], 'nonhttps':[]};

        if (config.env == 'development')
            valid_hosts.push('localhost:3000');

        md.use(iterator, 'verifyImageLinks', 'image', function(tokens, idx) {
            var link = tokens[idx].attrs.filter(function(x) { return x[0]=='src' });
            if (link.length>0) {
                var link = link[0][1];
                var verify_link = url.parse(link);
                if(valid_hosts.indexOf(verify_link.host) < 0 ) {
                    invalids['hosts'].push(link);
                }

                if(verify_link.protocol != 'https:') {
                    invalids['nonhttps'].push(link);
                }
            }
        });
        
        md.parseInline(markdown_text, {});
        return invalids;
    },


    convertImageLinks: function(markdown_text, project_token) {
        var up_dirname = config.upload_folder.split(path.sep).pop();
        var uptmp_dirname = config.upload_tmp.split(path.sep).pop();
        var regex = new RegExp("!\\[.*?\\]\\(.+?\\/("+uptmp_dirname+"\\/.+?)\\/.+?\\)", "g");

        markdown_text = markdown_text.replace(regex, function(a, b) {
            var result = a.replace(b, up_dirname+"/"+project_token);
            return result;
        });
        return markdown_text; 
    },

    // Builds the update query for the admin actions (project)
    getProjectUpdateQuery: function(service) {
        if (service == 'disabled') {
            return { state: 'disabled' };
        } else if (service == 'undisabled') {
            return{ state: 'pending approval' };
        } else if (service == 'approved') {
            return { state: 'launched' };
        } else if (service == 'unapproved') {
            return { state: 'pending approval' };
        } else if (service == 'freeworks') {
            return {'is_free_license': true };
        } else if (service == 'unfreeworks') {
            return { 'is_free_license': false };
        } else {
            return {};
        }
    },

    // Builds the where query for the admin actions (project)
    getProjectWhereQuery: function(service) {
        if (service == 'disabled') {
            return { state: 'disabled' };
        } else if (service == 'undisabled') {
            return{ state: { ne: 'disabled' } };
        } else if (service == 'approved') {
            return { state: 'launched' };
        } else if (service == 'unapproved') {
            return { state: 'pending approval' };
        } else if (service == 'freeworks') {
            return Sequelize.and({ 'is_free_license': true }, {'has_applied_free_license': true});
        } else if (service == 'unfreeworks') {
            return Sequelize.and({ 'is_free_license': false }, {'has_applied_free_license': true});
        } else {
            return {};
        }
    },

    // Builds the update query for the admin actions (user)
    getUserUpdateQuery: function(service) {
        if (service == 'disabled') {
            return { 'disabled': true };
        } else if (service == 'undisabled') {
            return{ 'disabled': false };
        } else if (service == 'approved') {
            return { 'is_approved': true };
        } else if (service == 'unapproved') {
            return { 'is_approved': false };
        } else if (service == 'tax_unapproved') {
            return { 'tax_exempt': false };
        } else if (service == 'tax_approved') {
            return { 'tax_exempt': true, 'has_applied_tax_exempt': false };
        } else {
            return {};
        }
    },

    // Builds the where query for the admin actions (user)
    getUserWhereQuery: function(service) {
        if (service == 'disabled') {
            return { 'disabled': true };
        } else if (service == 'undisabled') {
            return{ 'disabled': false };
        } else if (service == 'approved') {
            return { 'is_approved': true };
        } else if (service == 'unapproved') {
            return { 'is_approved': false };
        } else if (service == 'tax_unapproved') {
            return { 'has_applied_tax_exempt': true };
        } else if (service == 'tax_approved') {
            return { 'tax_exempt': true };
        } else {
            return {};
        }
    },

    // Get donation sum for a project for the current year
    getCurrentProjectDonations: function(project) {
        var start_year = new Date(new Date().getFullYear(), 0, 1);
        var end_year = new Date(new Date().getFullYear(), 11, 31);

        return models.donation.findAll({where: Sequelize.and({ projectId: project.id }, Sequelize.and({createdAt: {gte: start_year}}, {createdAt: {lte: end_year}})), attributes: [ [Sequelize.fn('SUM', Sequelize.col('total')), 'donation_sum']]});
    },
    

// Removing ES since it takes too much ram
/*
    // update projects when profile has been updated
    updateProjectsFromProfile: function(person) {
        var projects = person.projects;

        for (var i = 0; i<projects.length;i++) {
            (function(i) {
                module.exports.updateOrCreateElasticProject(projects[i], projects[i].category.name, person);
            })(i);
        }
    },

    // update or create elastic index
    updateOrCreateElasticProject: function(project, category_name, person) {
        elastic.es_client.index({
            index: config.elastic_index,
            type: 'project',
            id: project.token,
            body: {
                name: project.name,
                description: project.description,
                project_url: project.project_url,
                icon_url: project.icon_url,
                category: category_name,
                desired_funding: project.desired_funding,
                slug: project.slug,
                token: project.token,
                retired: project.retired,
                money_description: project.money_description,
                companies: project.companies,
                license: project.license,
                blurb: project.blurb,
                is_approved: project.is_approved,
                is_free_license: project.is_free_license,
                disabled: project.disabled,
                images: project.images,
                              
                // person stuff
                person_name: person.name,
                person_email: person.email,
                person_country_origin: person.country_origin,
                person_organization_type: person.organization_type,
                person_tax_deductions: person.tax_deductions,
                person_tax_exempt: person.tax_exempt,
                person_disabled: person.disabled,
            }
        });
    },
*/



    // delete file
    deleteFile: function(filepath) {
        fs.unlink(filepath, function(err) {
            console.log(err);
            // TODO: ignore error?
            if (err)
                return;
        });
    },

    validFileExtension: function(ext) {
        var valid_ext = ['.jpg', '.png'];
        if (valid_ext.indexOf(ext) >= 0) {
            return true;
        }
        return false;
    },

    // Returns upload dir + filename
    getUploadDir: function(project_token, filename) {
        return path.join(config.upload_folder + "/" + project_token + "/" + path.basename(filename));
    },

    // Returns tmp dir + filename
    getTmpDir: function(filename, person_token) {
        return path.join(config.upload_tmp + "/" + person_token + "/" + path.basename(filename));
    },

    // Returns just the tmp dir
    getTmpFolder: function(person_token) {
        return path.join(config.upload_tmp + "/" + person_token);
    },


    createPath: function(filepath, callback) {
        fs.mkdir(filepath, function(err) {
            if(err) {
                if(err.code == 'EEXIST') callback(null);   // already exists
                else callback(err);
            } else {
                callback(null);
            }
        });
    },

    // verify images exist in tmp folder
    moveFile: function(project_token, filename, person_token) {

        var log_id = uuid.v4();
        var log = logger.log.child({ log_id: log_id});
        log.info({project_token: project_token}, messages.log_begin_route('moveFile'));

        var ext = path.extname(filename);
        // check file extension
        if (module.exports.validFileExtension(ext) == true) {
            filename = sanitize(filename);
            var filepath_tmp = module.exports.getTmpDir(filename, person_token);
            var filepath_new = module.exports.getUploadDir(project_token, filename);
            // move file to new place
            fs.readFile(filepath_tmp, function(err, data) {
                if (err) { 
                    console.log("\n\n File read ERR \n\n");
                    log.warn({}, messages.log_fs_err('could not open file ' + filepath_tmp));
                    return;
                }
                // Ensure project token folder exists 
                fs.mkdir(path.dirname(filepath_new), function(err) {
                    if (err) {
                        console.log("\n\n File mkdir ERR \n\n");
                        if (err.code && err.code != 'EEXIST') {
                            log.warn({}, messages.log_fs_err('could not create path ' + path.dirname(filepath_new)));
                            return; 
                        }
                    }
                    fs.writeFile(filepath_new, data, function(err) {
                        if (err) { 
                            console.log("\n\n File write ERR \n\n");
                            log.warn({}, messages.log_fs_err('could not write to file ' + path.dirname(filepath_new)));
                            return;
                        }
                         
                        // delete old file
                        fs.unlink(filepath_tmp, function() {
                            console.log('written to ' + filepath_new);
                            if(err) {
                                console.log("\n\n File del ERR \n\n");
                                log.warn({}, messages.log_fs_err('could not remove old file ' + filepath_tmp));
                                return;
                            }
                            log.info({}, messages.log_fs_err('file moved success'));
                        });
                    });
                });
            });
        }
    },
};
